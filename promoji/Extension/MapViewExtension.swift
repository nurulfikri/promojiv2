//
//  MapViewExtension.swift
//  promoji
//
//  Created by Nurul Fikri on 8/30/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import MapKit

extension MKMapView {
    
    func regionFromAnnotations(_ annotations: [MKAnnotation]) -> MKCoordinateRegion {
        var region = MKCoordinateRegion()
        
        if annotations.count == 0 {
            region = MKCoordinateRegionMakeWithDistance(self.userLocation.coordinate, 1000, 1000)
        } else if annotations.count == 1 {
            let annotation = annotations.last
            region = MKCoordinateRegionMakeWithDistance(annotation!.coordinate, 0, 0)
            
            let miles = 1.0
            let scalingFactor = abs( (cos(2 * M_PI * annotation!.coordinate.latitude / 360.0) ))
            
            var span = MKCoordinateSpan()
            
            span.latitudeDelta = miles/69.0
            span.longitudeDelta = miles/(scalingFactor * 69.0)
            
            var region = MKCoordinateRegion()
            region.span = span
            region.center = annotation!.coordinate
            
            return region
        } else {
            var topLeftCoord = CLLocationCoordinate2D()
            topLeftCoord.latitude = -90;
            topLeftCoord.longitude = 180;
            
            var bottomRightCoord = CLLocationCoordinate2D()
            bottomRightCoord.latitude = 90;
            bottomRightCoord.longitude = -180;
            
            for annotation in annotations {
                topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude)
                topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude)
                bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude)
                bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude)
            }
            
            let extraSpace = 1.12
            region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) / 2.0
            region.center.longitude = topLeftCoord.longitude - (topLeftCoord.longitude - bottomRightCoord.longitude) / 2.0
            region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * extraSpace
            region.span.longitudeDelta = fabs(topLeftCoord.longitude - bottomRightCoord.longitude) * extraSpace
            
            return self.regionThatFits(region)
        }
        return region
    }
}
