//
//  PromoGroupViewController.swift
//  promoji
//
//  Created by Nurul Fikri on 8/30/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class PromoGroupViewController: CoreViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tblView: UITableView!
    var header : StretchHeader!
    var navigationView = UIView()
    
    override func viewDidLoad() {
        setupHeaderView()
        self.tblView.contentInset.top = -20
        // NavigationHeader
        let navibarHeight : CGFloat = navigationController!.navigationBar.bounds.height
        let statusbarHeight : CGFloat = UIApplication.shared.statusBarFrame.size.height
        navigationView = UIView()
        navigationView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: navibarHeight + statusbarHeight)
        navigationView.backgroundColor = UIColor(red: 43.0/255.0, green: 179.0/255.0, blue: 58.0/255.0, alpha: 1)
        navigationView.alpha = 0.0
        view.addSubview(navigationView)
        
        let image = UIImageView()
        image.frame = navigationView.frame
        image.contentMode = UIViewContentMode.scaleAspectFill
        image.clipsToBounds = true
        image.sd_setImage(with: URL(string:"http://previews.123rf.com/images/sellingpix/sellingpix1409/sellingpix140900047/31788840-Oktoberfest-festival-de-la-tipograf-a-de-estilo-retro-del-cartel-del-vector-del-dise-o-de-la-vendimi-Foto-de-archivo.jpg")!)
        navigationView.addSubview(image)
        
        // back button
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 10, y: 20, width: 44, height: 44)
        button.setImage(UIImage(named: "back-white-normal")?.withRenderingMode(.alwaysTemplate), for: UIControlState())
        button.tintColor = UIColor.white
        button.addTarget(self, action: #selector(PromoDetailViewController.leftButtonAction), for: .touchUpInside)
        view.addSubview(button)
    }
    
    func setupHeaderView() {
        let options = StretchHeaderOptions()
        options.position = .fullScreenTop
        
        header = StretchHeader()
        header.stretchHeaderSize(headerSize: CGSize(width: view.frame.size.width, height: 200),
                                 imageSize: CGSize(width: view.frame.size.width, height: 200),
                                 controller: self,
                                 options: options)
        header.imageView.sd_setImage(with: URL(string: "http://previews.123rf.com/images/sellingpix/sellingpix1409/sellingpix140900047/31788840-Oktoberfest-festival-de-la-tipograf-a-de-estilo-retro-del-cartel-del-vector-del-dise-o-de-la-vendimi-Foto-de-archivo.jpg"))
        tblView.tableHeaderView = header
    }

    // MARK: - Selector
    func leftButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    // MARK: - ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        header.updateScrollViewOffset(scrollView)
        let offset : CGFloat = scrollView.contentOffset.y
        if (offset > 50) {
            let alpha : CGFloat = min(CGFloat(1), CGFloat(1) - (CGFloat(50) + (navigationView.frame.height) - offset) / (navigationView.frame.height))
            navigationView.alpha = CGFloat(alpha)
            
        } else {
            navigationView.alpha = 0.0;
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as! SmallPromoCellView).buildLayout()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SmallPromoCellView? = self.tblView.dequeueReusableCell(withIdentifier: "smallPromoCellView", for: indexPath) as? SmallPromoCellView
        cell?.merchantImage.sd_setImage(with: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxMPTIVLiNwGonPvjeOVpR5fWnLCvfeCqSsrAhAXsbs0r3Wg1JGw"))
        cell?.merchantPromo = "Sipenulis"
        cell?.promoterString = "Fikri"
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        toPromoDetail()
    }
}

