//
//  PromoCategoryDetailViewController.swift
//  promoji
//
//  Created by Nurul Fikri on 8/31/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class PromoCategoryDetailViewController: CoreViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tblView: UITableView!
//    var navigationView = UIView()
    var header : StretchHeader!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupHeaderView()
        
        self.title = "Book"
    }
    
    override func initNavigationBar() {
        if let topItem = self.navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        }
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        let ver = UIDevice.current.systemVersion.components(separatedBy: ".")
        if Int(ver[0])! >= 7 {
            self.navigationController?.navigationBar.barTintColor = UIColor.orange
            self.navigationController?.navigationBar.isTranslucent = false
        } else {
            self.navigationController?.navigationBar.tintColor = UIColor.orange
        }
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
    func setupHeaderView() {
        let options = StretchHeaderOptions()
        options.position = .fullScreenTop
        
        header = StretchHeader()
        header.stretchHeaderSize(headerSize: CGSize(width: view.frame.size.width, height: 150),
                                 imageSize: CGSize(width: view.frame.size.width, height: 70),
                                 controller: self,
                                 options: options)
        header.imageView.backgroundColor = UIColor.orange
        header.backgroundColor = UIColor.orange
        
        let imageView = UIImageView(frame: CGRect(x: (UIScreen.main.bounds.width-100)/2, y: 32, width: 100, height: 100))
        imageView.sd_setImage(with: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxMPTIVLiNwGonPvjeOVpR5fWnLCvfeCqSsrAhAXsbs0r3Wg1JGw"))
        header.addSubview(imageView)
        
        tblView.tableHeaderView = header
    }
    
    func action(_ sender: SegmentedControl){
        print("sender: \(sender.selectedIndex)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 20
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath as NSIndexPath).section == 0 {
            if (indexPath as NSIndexPath).row == 0 {
                return 120
            } else {
                return 130
            }
        } else {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let sectionView = UIView(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.width,height: 60))
            sectionView.backgroundColor = UIColor.orange
            
            let screen = UIScreen.main.bounds
            
            let segmentedControl = SegmentedControl.init(
                FrameWithoutIcon: CGRect(x: 8, y: 8, width: screen.width - 16, height: 44),
                items: ["All", "In-Store", "Online"],
                backgroundColor: UIColor.darkGray,
                thumbColor: UIColor.orange,
                textColor: UIColor.gray,
                selectedTextColor: UIColor.white)
            
            segmentedControl.selectedIndex = 0
            
            segmentedControl.addTarget(self, action: #selector(self.action(_:)), for: .valueChanged)
            segmentedControl.layoutIfNeeded()
            sectionView.addSubview(segmentedControl)
            return sectionView
        } else {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 1))
            view.backgroundColor = UIColor.gray
            return view
        }
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 60
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).section == 0 {
            if (indexPath as NSIndexPath).row == 0 {
                
            } else {
                (cell as! SmallPromoCellView).buildLayout()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath as NSIndexPath).section == 0 {
            if (indexPath as NSIndexPath).row == 0 {
                let cell: SubscribeHomeCellView? = self.tblView.dequeueReusableCell(withIdentifier: "subscribeHomeCellView") as? SubscribeHomeCellView
                cell?.subscribtionCollectionView.delegate = self
                cell?.subscribtionCollectionView.dataSource = self
                return cell!
            } else {
                let cell: SmallPromoCellView? = self.tblView.dequeueReusableCell(withIdentifier: "smallPromoCellView", for: indexPath) as? SmallPromoCellView
                cell?.merchantImage.sd_setImage(with: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxMPTIVLiNwGonPvjeOVpR5fWnLCvfeCqSsrAhAXsbs0r3Wg1JGw"))
                cell?.merchantPromo = "Sipenulis"
                cell?.promoterString = "Fikri"
                return cell!
            }
        } else {
            let cell = UITableViewCell()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).section == 0 {
            if (indexPath as NSIndexPath).row != 0 {
                toPromoDetail()
            }
        }
    }
    
    func brandTapped(_ sender: UIImageView) {
        toBrandDetail()
    }
    
    // MARK: - ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        header.updateScrollViewOffset(scrollView)
    }
    
}

extension PromoCategoryDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 15
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subscriptionCell", for: indexPath) as! SubscriptionHeaderCellView
        
        cell.imageBrand.sd_setImage(with: URL(string: "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTDWjCgZ721_Ij7PjA-f7x_Sr8TN8trGo5ABY2iOeHLY5o8et78ng"))
        
        cell.layer.cornerRadius = 8
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\((indexPath as NSIndexPath).item)!")
        toBrandDetail()
    }
}
