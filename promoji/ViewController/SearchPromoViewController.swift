//
//  SearchPromoViewController.swift
//  promoji
//
//  Created by Nurul Fikri on 8/31/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class SearchPromoViewController: CoreViewController, UITableViewDelegate, UITableViewDataSource, UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bgTop: UIView!
    var searchController : UISearchController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        bgTop.backgroundColor = UIColor(red: 43.0/255.0, green: 179.0/255.0, blue: 58.0/255.0, alpha: 1)
        
        let screen = UIScreen.main.bounds
        
        let segmentedControl = SegmentedControl.init(
            FrameWithoutIcon: CGRect(x: 8, y: 8, width: screen.width - 16, height: 38),
            items: ["All", "In-Store", "Online"],
            backgroundColor: UIColor.gray,
            thumbColor: UIColor(red: 43.0/255.0, green: 179.0/255.0, blue: 58.0/255.0, alpha: 1),
            textColor: UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.6),
            selectedTextColor: UIColor.white)
        
        segmentedControl.selectedIndex = 0
        
        segmentedControl.addTarget(self, action: #selector(self.action(_:)), for: .valueChanged)
        segmentedControl.layoutIfNeeded()
        bgTop.addSubview(segmentedControl)
        
        initSearchHeader()
    }
    
    func action(_ sender: SegmentedControl){
        print("sender: \(sender.selectedIndex)")
    }
    
    func initSearchHeader() {
        self.searchController = UISearchController(searchResultsController:  nil)
        
        self.searchController.searchResultsUpdater = self
        self.searchController.delegate = self
        self.searchController.searchBar.delegate = self
        
        self.searchController.searchBar.showsCancelButton = true
        
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = true
        self.navigationItem.titleView = searchController.searchBar
        self.definesPresentationContext = true
    }
    
    func presentSearchController(_ searchController: UISearchController) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in }, completion: { (completed) -> Void in
            self.searchController.searchBar.becomeFirstResponder()
        }) 
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.searchController.isActive = true
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as! SmallPromoCellView).buildLayout()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SmallPromoCellView? = self.tblView.dequeueReusableCell(withIdentifier: "smallPromoCellView", for: indexPath) as? SmallPromoCellView
        cell?.merchantImage.sd_setImage(with: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxMPTIVLiNwGonPvjeOVpR5fWnLCvfeCqSsrAhAXsbs0r3Wg1JGw"))
        cell?.merchantPromo = "Sipenulis"
        cell?.promoterString = "Fikri"
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        toPromoDetail()
    }
    
}
