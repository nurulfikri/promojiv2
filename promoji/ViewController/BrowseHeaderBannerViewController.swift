//
//  BrowseHeaderBannerViewController.swift
//  promoji
//
//  Created by Nurul Fikri on 8/24/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class BrowseHeaderBannerViewController: CoreViewController {
    var itemIndex: Int = 0
    var imageURL: NSURL = NSURL() {
        
        didSet {
            
            if let imageView = imageFeatured {
                imageView.sd_setImage(with: imageURL as URL!)
            }
            
        }
        
    }
    @IBOutlet var imageFeatured: UIImageView?
    override func viewDidLoad() {
        super.viewDidLoad()
        imageFeatured!.sd_setImage(with: imageURL as URL!)
    }
    
}
