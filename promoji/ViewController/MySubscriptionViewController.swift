//
//  MySubscriptionViewController.swift
//  promoji
//
//  Created by Nurul Fikri on 9/10/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation


class MySubscriptionViewController: CoreViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bgTop: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        bgTop.backgroundColor = UIColor(red: 43.0/255.0, green: 179.0/255.0, blue: 58.0/255.0, alpha: 1)
        
        let screen = UIScreen.main.bounds
        
        let segmentedControl = SegmentedControl.init(
            FrameWithoutIcon: CGRect(x: 8, y: 8, width: screen.width - 16, height: 38),
            items: ["All", "In-Store", "Online"],
            backgroundColor: UIColor.gray,
            thumbColor: UIColor(red: 43.0/255.0, green: 179.0/255.0, blue: 58.0/255.0, alpha: 1),
            textColor: UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.6),
            selectedTextColor: UIColor.white)
        
        segmentedControl.selectedIndex = 0
        
        segmentedControl.addTarget(self, action: #selector(self.action(_:)), for: .valueChanged)
        segmentedControl.layoutIfNeeded()
        bgTop.addSubview(segmentedControl)
        
    }
    
    func action(_ sender: SegmentedControl){
        print("sender: \(sender.selectedIndex)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as! MySubscriptionTableCellView).buildLayout()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MySubscriptionTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "mySubscriptionTableCellView", for: indexPath) as? MySubscriptionTableCellView
        
        cell?.iconView.sd_setImage(with: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxMPTIVLiNwGonPvjeOVpR5fWnLCvfeCqSsrAhAXsbs0r3Wg1JGw"))
        cell?.lblTitle.text = "Sipenulis"
        cell?.lblSubscription.text = "Subscribed"
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
