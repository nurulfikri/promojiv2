//
//  MainViewController.swift
//  promoji
//
//  Created by Nurul Fikri on 7/27/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class MainViewController: CoreViewController, UITabBarControllerDelegate {
    
    var mainViewTabBarController: UITabBarController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTabBar()
    }
    
    func initTabBar() {
        
        self.mainViewTabBarController = UITabBarController()
        self.mainViewTabBarController?.delegate = self
        
        self.mainViewTabBarController?.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        self.mainViewTabBarController?.tabBar.backgroundImage = UIImage(named: "tabbar")
        self.mainViewTabBarController?.tabBar.selectionIndicatorImage = UIImage(named: "tabbar-selected")
        self.mainViewTabBarController?.tabBar.shadowImage = UIImage()
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let searchViewController = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        let recommendationViewController = storyboard.instantiateViewController(withIdentifier: "RecommendationViewController") as! RecommendationViewController
        let locationViewController = storyboard.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
        let moreViewController = storyboard.instantiateViewController(withIdentifier: "MoreViewController") as! MoreViewController
        
        let navigationControllerHome = ScrollingNavigationController(rootViewController: homeViewController)
        let navigationControllerSearch = ScrollingNavigationController(rootViewController: searchViewController)
        let navigationControllerFav = ScrollingNavigationController(rootViewController: recommendationViewController)
        let navigationControllerLoc = ScrollingNavigationController(rootViewController: locationViewController)
        let navigationControllerMore = ScrollingNavigationController(rootViewController: moreViewController)
        
        self.mainViewTabBarController!.viewControllers = [navigationControllerHome, navigationControllerSearch, navigationControllerFav, navigationControllerLoc, navigationControllerMore]
        self.mainViewTabBarController?.viewWillLayoutSubviews()
        self.view.addSubview(self.mainViewTabBarController!.view)
        
        for i in 0 ..< 5 {
            let tabBarItem = self.mainViewTabBarController?.tabBar.items![i]
            tabBarItem!.imageInsets = UIEdgeInsetsMake(5.0, 0.0, -5.0, 0.0)
            
            var normalImage = UIImage()
            var selectedImage = UIImage()
            
            switch i {
            case 0:
                normalImage = UIImage(named: "tabicon-home-normal")!
                selectedImage = UIImage(named: "tabicon-home-active")!
                break
            case 1:
                normalImage = UIImage(named: "tabicon-browse-normal")!
                selectedImage = UIImage(named: "tabicon-browse-active")!
                break
            case 2:
                normalImage = UIImage(named: "tabicon-browse-normal")!
                selectedImage = UIImage(named: "tabicon-browse-active")!
                break
            case 3:
                normalImage = UIImage(named: "tabicon-place-normal")!
                selectedImage = UIImage(named: "tabicon-place-active")!
                break
            case 4:
                normalImage = UIImage(named: "tabicon-more-normal")!
                selectedImage = UIImage(named: "tabicon-more-active")!
                break
            default :
                break
            }
            
            normalImage = normalImage.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            selectedImage = normalImage.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            tabBarItem!.selectedImage = selectedImage
            tabBarItem!.image = normalImage
            
        }
    }
    
}
