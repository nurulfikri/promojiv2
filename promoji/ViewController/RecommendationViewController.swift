//
//  RecommendationViewController.swift
//  promoji
//
//  Created by Nurul Fikri on 7/27/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

var numberOfCards: UInt = 10
class RecommendationViewController: CoreViewController {
    
    @IBOutlet weak var kolodaView: KolodaView!
    
    var dataSource: Array<URL> = {
        var array: Array<URL> = []
        
        array.append(URL(string:"http://previews.123rf.com/images/sellingpix/sellingpix1409/sellingpix140900047/31788840-Oktoberfest-festival-de-la-tipograf-a-de-estilo-retro-del-cartel-del-vector-del-dise-o-de-la-vendimi-Foto-de-archivo.jpg")!)
        array.append(URL(string:"https://s-media-cache-ak0.pinimg.com/236x/f7/8c/9a/f78c9a75a989652c2873a0ab2cd66beb.jpg")!)
        array.append(URL(string:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT_wBPiymmFuJyJFaN1h7qMzZP6xyywgZRnZoempwbTTmA1dxCx")!)
        array.append(URL(string:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQML8K1MF0tVJ0xG5ibJIcVXROGOgvuPru4XS592BjyYDQTuReT")!)
        array.append(URL(string:"https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRTAmbJtZpe3eLr6IdESQW-bb6fHO8FZ65R6ZHIE8IM0jVD1XHOy1twpjs")!)
        array.append(URL(string:"https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcT25v0Vu7CrKswR9yWRPrXCO2JgtqhueWDgLXKDLnI8VwBWpPKS")!)
        array.append(URL(string:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQGl3VyvCle0ar5Hoc12CeOMZmXlMnnlioxam43Y9Znm-fO0Nup")!)
        array.append(URL(string:"https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSpL-YbhEZyMxDIYcWAfi4HYgm3mw6IdLyBAB9SR9KO5W-fPWdb")!)
        array.append(URL(string:"https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSvGvw69jDzGVUQN5OA-pCame3dlyDE0szlGtV-QBoCpOye1OkA")!)
        array.append(URL(string:"http://previews.123rf.com/images/sellingpix/sellingpix1409/sellingpix140900047/31788840-Oktoberfest-festival-de-la-tipograf-a-de-estilo-retro-del-cartel-del-vector-del-dise-o-de-la-vendimi-Foto-de-archivo.jpg")!)
        
        return array
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initPopupInfo(UIImage(named: "tabicon-browse-normal")!, title: "Recommended For You", description: "We recommend you promos based on your subscription & promos you mojied.")
        showPopupInfo()
        
        kolodaView.dataSource = self
        kolodaView.delegate = self
        
        self.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func shufflePressed(_ sender: AnyObject) {
        kolodaView?.revertAction()
    }
    @IBAction func noPressed(_ sender: AnyObject) {
        kolodaView?.swipe(SwipeResultDirection.Left)
    }
    @IBAction func mojiPressed(_ sender: AnyObject) {
        kolodaView?.swipe(SwipeResultDirection.Right)
    }
    @IBAction func sharePressed(_ sender: AnyObject) {
    }
    
}

extension RecommendationViewController: KolodaViewDelegate {
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        dataSource.insert(URL(string:"http://previews.123rf.com/images/sellingpix/sellingpix1409/sellingpix140900047/31788840-Oktoberfest-festival-de-la-tipograf-a-de-estilo-retro-del-cartel-del-vector-del-dise-o-de-la-vendimi-Foto-de-archivo.jpg")!, at: kolodaView.currentCardIndex - 1)
        let position = kolodaView.currentCardIndex
        kolodaView.insertCardAtIndexRange(position..<position+1, animated: true)
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAtIndex index: UInt) {
        
    }
}

extension RecommendationViewController: KolodaViewDataSource {
    
    func kolodaNumberOfCards(_ koloda:KolodaView) -> UInt {
        return UInt(dataSource.count)
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAtIndex index: UInt) -> UIView {
        let image = UIImageView()
        image.sd_setImage(with: dataSource[Int(index)])
        return image
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAtIndex index: UInt) -> OverlayView? {
        return Bundle.main.loadNibNamed("OverlayView", owner: self, options: nil)?[0] as? OverlayView
    }
}
