//
//  HomeViewController.swift
//  promoji
//
//  Created by Nurul Fikri on 7/27/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class HomeViewController: CoreViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initPopupInfo(UIImage(named: "tabicon-home-normal")!, title: "This is your Home", description: "You'll see anything posted by the brands you subscribed.")
        showPopupInfo()
        
        self.title = "Promoji"
        
        self.tblView.register(UINib(nibName: "PromoCardCellView", bundle: nil), forCellReuseIdentifier: "PromoCardCellView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let navigationController = navigationController as? ScrollingNavigationController {
            navigationController.followScrollView(self.tblView, delay: 50.0)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let navigationController = navigationController as? ScrollingNavigationController {
            navigationController.showNavbar(animated: true)
        }
    }
    
    @IBAction func touchViewAll(_ sender: AnyObject) {
        NSLog("View All")
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let cell: SubscribeHomeCellView? = self.tblView.dequeueReusableCell(withIdentifier: "subscribeHomeCellView") as? SubscribeHomeCellView
            return cell!
        } else {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 1))
            view.backgroundColor = UIColor.gray
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 108
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return 20
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 465
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as! PromoCardCellView).buildLayout()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath as NSIndexPath).section == 0 {
            let cell: PromoCardCellView? = self.tblView.dequeueReusableCell(withIdentifier: "PromoCardCellView", for: indexPath) as? PromoCardCellView
            cell?.bigPicture.sd_setImage(with: URL(string: "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSQHv6Wvso7emdD05ulmXJNo7sVs3rIoSyqiTlnA_9YwoXns_pi"))
            cell?.merchantImage.sd_setImage(with: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxMPTIVLiNwGonPvjeOVpR5fWnLCvfeCqSsrAhAXsbs0r3Wg1JGw"))
            cell?.merchantPromo = "Sipenulis"
            cell?.promoterString = "Fikri"
            
            let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(HomeViewController.imageTapped(_:)))
            cell?.bigPicture.isUserInteractionEnabled = true
            cell?.bigPicture.tag = (indexPath as NSIndexPath).row
            cell?.bigPicture.addGestureRecognizer(tapGestureRecognizer)
            
            let tapBrandGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(HomeViewController.brandTapped(_:)))
            cell?.merchantImage.isUserInteractionEnabled = true
            cell?.merchantImage.tag = (indexPath as NSIndexPath).row
            cell?.merchantImage.addGestureRecognizer(tapBrandGestureRecognizer)
            
            return cell!
        } else {
            let cell: PromoCardCellView? = self.tblView.dequeueReusableCell(withIdentifier: "PromoCardCellView", for: indexPath) as? PromoCardCellView
            cell?.bigPicture.sd_setImage(with: URL(string: "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSQHv6Wvso7emdD05ulmXJNo7sVs3rIoSyqiTlnA_9YwoXns_pi"))
            cell?.merchantImage.sd_setImage(with: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxMPTIVLiNwGonPvjeOVpR5fWnLCvfeCqSsrAhAXsbs0r3Wg1JGw"))
            cell?.merchantPromo = "Sipenulis"
            cell?.promoterString = "Fikri"
            
            let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(HomeViewController.imageTapped(_:)))
            cell?.bigPicture.isUserInteractionEnabled = true
            cell?.bigPicture.tag = (indexPath as NSIndexPath).row + 1
            cell?.bigPicture.addGestureRecognizer(tapGestureRecognizer)
            
            let tapBrandGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(HomeViewController.brandTapped(_:)))
            cell?.merchantImage.isUserInteractionEnabled = true
            cell?.merchantImage.tag = (indexPath as NSIndexPath).row
            cell?.merchantImage.addGestureRecognizer(tapBrandGestureRecognizer)
            
            return cell!
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    func imageTapped(_ sender: UIImageView) {
        toPromoDetail()
    }
    
    func brandTapped(_ sender: UIImageView) {
        toBrandDetail()
    }
    
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 15
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subscriptionCell", for: indexPath) as! SubscriptionHeaderCellView
        
        cell.imageBrand.sd_setImage(with: URL(string: "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTDWjCgZ721_Ij7PjA-f7x_Sr8TN8trGo5ABY2iOeHLY5o8et78ng"))
        
        cell.layer.cornerRadius = 8
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\((indexPath as NSIndexPath).item)!")
        toBrandDetail()
    }
}
