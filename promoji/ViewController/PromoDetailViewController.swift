//
//  PromoDetailViewController.swift
//  promoji
//
//  Created by Nurul Fikri on 8/30/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class PromoDetailViewController: CoreViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tblView: UITableView!
    var navigationView = UIView()
    
    let slideView = UIView()
    let indicatorHolderView = UIView()
    let indicatorView = UIView()
    let headerBanner = UIView()
    
    var images = [SKPhoto]()
    
    var timer : Timer?
    
    var headerScrollView = UIScrollView()
    var pageViews: [UIImageView?] = []
    
    var arrayLocation = NSMutableArray()
    var arrayBrandsRelated = NSMutableArray()
    
    let contentTempTOC = "<li>Bila diperlukan, QEON berhak untuk mengambil tindakan hukum terhadap pelanggaran End User License Agreement ini ataupun terhadap tindakan yang dianggap merugikan perusahaan selaku pemegang hak paten atas jasa layanan.</li><li>Bila diperlukan, QEON berhak untuk mengambil tindakan hukum terhadap pelanggaran End User License Agreement ini ataupun terhadap tindakan yang dianggap merugikan perusahaan selaku pemegang hak paten atas jasa layanan.</li><li>Bila diperlukan, QEON berhak untuk mengambil tindakan hukum terhadap pelanggaran End User License Agreement ini ataupun terhadap tindakan yang dianggap merugikan perusahaan selaku pemegang hak paten atas jasa layanan.</li><li>Bila diperlukan, QEON berhak untuk mengambil tindakan hukum terhadap pelanggaran End User License Agreement ini ataupun terhadap tindakan yang dianggap merugikan perusahaan selaku pemegang hak paten atas jasa layanan.</li><li>Bila diperlukan, QEON berhak untuk mengambil tindakan hukum terhadap pelanggaran End User License Agreement ini ataupun terhadap tindakan yang dianggap merugikan perusahaan selaku pemegang hak paten atas jasa layanan.</li>"
    
    var dataSource: Array<URL> = {
        var array: Array<URL> = []
        
        array.append(URL(string:"http://previews.123rf.com/images/sellingpix/sellingpix1409/sellingpix140900047/31788840-Oktoberfest-festival-de-la-tipograf-a-de-estilo-retro-del-cartel-del-vector-del-dise-o-de-la-vendimi-Foto-de-archivo.jpg")!)
        array.append(URL(string:"https://s-media-cache-ak0.pinimg.com/236x/f7/8c/9a/f78c9a75a989652c2873a0ab2cd66beb.jpg")!)
        array.append(URL(string:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT_wBPiymmFuJyJFaN1h7qMzZP6xyywgZRnZoempwbTTmA1dxCx")!)
        array.append(URL(string:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQML8K1MF0tVJ0xG5ibJIcVXROGOgvuPru4XS592BjyYDQTuReT")!)
        array.append(URL(string:"https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRTAmbJtZpe3eLr6IdESQW-bb6fHO8FZ65R6ZHIE8IM0jVD1XHOy1twpjs")!)
        array.append(URL(string:"https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcT25v0Vu7CrKswR9yWRPrXCO2JgtqhueWDgLXKDLnI8VwBWpPKS")!)
        array.append(URL(string:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQGl3VyvCle0ar5Hoc12CeOMZmXlMnnlioxam43Y9Znm-fO0Nup")!)
        array.append(URL(string:"https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSpL-YbhEZyMxDIYcWAfi4HYgm3mw6IdLyBAB9SR9KO5W-fPWdb")!)
        array.append(URL(string:"https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSvGvw69jDzGVUQN5OA-pCame3dlyDE0szlGtV-QBoCpOye1OkA")!)
        array.append(URL(string:"http://previews.123rf.com/images/sellingpix/sellingpix1409/sellingpix140900047/31788840-Oktoberfest-festival-de-la-tipograf-a-de-estilo-retro-del-cartel-del-vector-del-dise-o-de-la-vendimi-Foto-de-archivo.jpg")!)
        
        return array
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initHeaderScroll()
        
        for url in dataSource {
            let photo = SKPhoto.photoWithImageURL(url.absoluteString)
            photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
            images.append(photo)
        }
        
        self.tblView.contentInset.top = -20
        // NavigationHeader
        let navibarHeight : CGFloat = navigationController!.navigationBar.bounds.height
        let statusbarHeight : CGFloat = UIApplication.shared.statusBarFrame.size.height
        navigationView = UIView()
        navigationView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: navibarHeight + statusbarHeight)
        navigationView.backgroundColor = UIColor(red: 43.0/255.0, green: 179.0/255.0, blue: 58.0/255.0, alpha: 1)
        navigationView.alpha = 0.0
        view.addSubview(navigationView)
        
        // back button
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 10, y: 20, width: 44, height: 44)
        button.setImage(UIImage(named: "back-white-normal")?.withRenderingMode(.alwaysTemplate), for: UIControlState())
        button.tintColor = UIColor.white
        button.addTarget(self, action: #selector(PromoDetailViewController.leftButtonAction), for: .touchUpInside)
        view.addSubview(button)
        
        arrayLocation.add("Citywalk")
        arrayLocation.add("Grand Indonesia")
        arrayLocation.add("Pacific Place")
        
        arrayBrandsRelated.add("Dunkin Donuts")
        arrayBrandsRelated.add("Mc Donalds")
        arrayBrandsRelated.add("KFC")
        arrayBrandsRelated.add("A&W")
        arrayBrandsRelated.add("Rocky")
        arrayBrandsRelated.add("Ayam Taliwang")
    }
    
    // MARK: - Selector
    func leftButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (indexPath as NSIndexPath).row {
        case 0:
            return 109
        case 1:
            return 64
        case 2:
            return 88
        case 3:
            return 88
        case 4:
            let height = "lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ".heightWithConstrainedWidth(UIScreen.main.bounds.width - 16, font: UIFont.systemFont(ofSize: 14))
            return 34 + height
        case 5:
            let height = contentTempTOC.heightWithConstrainedWidth(UIScreen.main.bounds.width - 16, font: UIFont.systemFont(ofSize: 14))
            return 34 + height
        case 6:
            return 196 + 42 + (CGFloat(arrayLocation.count > 5 ? 5 : arrayLocation.count) * 46) + 16
        case 7:
            return 52 + 42 + (CGFloat(arrayBrandsRelated.count > 5 ? 5 : arrayBrandsRelated.count) * 72) + 16
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch (indexPath as NSIndexPath).row {
        case 0:
            (cell as! PromoCaptionTableCellView).buildLayout()
        case 1:
            (cell as! PromoDetailShopNowTableCellView).buildLayout()
        case 2:
            (cell as! BrandDetailsRelatedBrandTableCellView).buildLayout()
        case 3:
            (cell as! PromoDetailsSecondInfoTableCellView).buildLayout()
        case 4:
            (cell as! BrandDetailsAboutTableCellView).buildLayout()
        case 5:
            (cell as! PromoDetailsTermConditionTableCellView).buildLayout()
        case 6:
            (cell as! BrandDetailsLocationTableViewCell).buildLayout()
        case 7:
            (cell as! BrandDetailsRelatedTableCellView).buildLayout()
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath as NSIndexPath).row {
        case 0:
            let cell: PromoCaptionTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "promoCaptionTableCellView", for: indexPath) as? PromoCaptionTableCellView
            return cell!
        case 1:
            let cell: PromoDetailShopNowTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "promoDetailShopNowTableCellView", for: indexPath) as? PromoDetailShopNowTableCellView
            return cell!
        case 2:
            let cell: BrandDetailsRelatedBrandTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "brandDetailsRelatedBrandTableCellView", for: indexPath) as? BrandDetailsRelatedBrandTableCellView
            cell?.lblBrand.text = "Sipenulis"
            cell?.imageBrand.sd_setImage(with: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxMPTIVLiNwGonPvjeOVpR5fWnLCvfeCqSsrAhAXsbs0r3Wg1JGw"))
            
            cell?.bgView.layer.cornerRadius = 8.0
            cell?.bgView.layer.masksToBounds = true
            return cell!
        case 3:
            let cell: PromoDetailsSecondInfoTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "promoDetailsSecondInfoTableCellView", for: indexPath) as? PromoDetailsSecondInfoTableCellView
            return cell!
        case 4:
            let cell: BrandDetailsAboutTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "brandDetailsAboutTableCellView", for: indexPath) as? BrandDetailsAboutTableCellView
            cell?.lblAbout.text = "lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum "
            return cell!
        case 5:
            let cell: PromoDetailsTermConditionTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "promoDetailsTermConditionTableCellView", for: indexPath) as? PromoDetailsTermConditionTableCellView
            cell?.txtContent.attributedText = contentTempTOC.html2AttributedString
            return cell!
        case 6:
            let cell: BrandDetailsLocationTableViewCell? = self.tblView.dequeueReusableCell(withIdentifier: "brandDetailsLocationTableViewCell", for: indexPath) as? BrandDetailsLocationTableViewCell
            cell?.arrayLocation = arrayLocation
            return cell!
        case 7:
            let cell: BrandDetailsRelatedTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "brandDetailsRelatedTableCellView", for: indexPath) as? BrandDetailsRelatedTableCellView
            cell?.arrayRelated = arrayBrandsRelated
            return cell!
        default:
            let cell: BrowseMainTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "browseMainTableCellView", for: indexPath) as? BrowseMainTableCellView
            return cell!
        }
    }
    
}

extension PromoDetailViewController: UIScrollViewDelegate {
    func initHeaderScroll() {
        // 3
        for _ in 0..<dataSource.count {
            pageViews.append(nil)
        }
        
        headerBanner.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width)
        headerBanner.backgroundColor = UIColor.white
        
        headerScrollView.showsHorizontalScrollIndicator = false
        headerScrollView.frame = headerBanner.frame
        headerScrollView.delegate = self
        headerScrollView.isPagingEnabled = true
        headerScrollView.contentSize = CGSize(width: headerBanner.frame.size.width * CGFloat(dataSource.count),
                                        height: 0)
        headerBanner.addSubview(headerScrollView)
        
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(PromoDetailViewController.goNextPage), userInfo: nil, repeats: true)
        
        indicatorHolderView.frame = CGRect(x: 0, y: headerBanner.frame.size.height - 4, width: headerBanner.frame.size.width, height: 4)
        indicatorHolderView.backgroundColor = UIColor(white: 0.0, alpha: 0.4)
        indicatorView.frame = CGRect(x: 0, y: 0, width: indicatorHolderView.frame.width/CGFloat(dataSource.count), height: 4)
        indicatorView.backgroundColor = UIColor(white: 1.0, alpha: 0.75)
        indicatorHolderView.addSubview(indicatorView)
        headerBanner.addSubview(indicatorHolderView)
        
        tblView.tableHeaderView = headerBanner
        
        loadVisiblePages()
    }
    
    
    func goNextPage() {
        if dataSource.count > 1 {
            let pageWidth = UIScreen.main.bounds.size.width
            let page = Int(floor((headerScrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
            
            if page+1 < dataSource.count {
                UIView.animate(withDuration: 0.2, animations: {
                    self.headerScrollView.contentOffset.x = self.headerScrollView.contentOffset.x + pageWidth
                })
                
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.headerScrollView.contentOffset.x = 0
                })
            }
        }
        
    }

    // MARK: - ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tblView {
            // NavigationHeader alpha update
            let offset : CGFloat = scrollView.contentOffset.y
            if (offset > 50) {
                let alpha : CGFloat = min(CGFloat(1), CGFloat(1) - (CGFloat(50) + (navigationView.frame.height) - offset) / (navigationView.frame.height))
                navigationView.alpha = CGFloat(alpha)
                
            } else {
                navigationView.alpha = 0.0;
            }
        } else if scrollView == headerScrollView {
            loadVisiblePages()
        }
        
        
    }
    
    func loadPage(_ page: Int) {
        if page < 0 || page >= dataSource.count {
            // If it's outside the range of what you have to display, then do nothing
            return
        }
        
        // 1
        if pageViews[page] != nil {
            
        } else {
            let screenSize: CGRect = UIScreen.main.bounds
            let screenWidth = screenSize.width
            var frame = headerScrollView.frame
            frame.size.width=screenWidth
            frame.origin.x = screenWidth * CGFloat(page)
            frame.origin.y = 0.0
            
            let newPageView = UIImageView()
            newPageView.sd_setImage(with: dataSource[page])
            newPageView.contentMode = .scaleAspectFill
            newPageView.clipsToBounds = true
            newPageView.frame = frame
            newPageView.isUserInteractionEnabled = true
            newPageView.tag = page
            let gestureRecognize = UITapGestureRecognizer(target: self, action: #selector(PromoDetailViewController.selectRow(_:)))
            newPageView.addGestureRecognizer(gestureRecognize)
            
            headerScrollView.addSubview(newPageView)
            
            pageViews[page] = newPageView
        }
    }
    func purgePage(_ page: Int) {
        if page < 0 || page >= dataSource.count {
            // If it's outside the range of what you have to display, then do nothing
            return
        }
        
        // Remove a page from the scroll view and reset the container array
        if let pageView = pageViews[page] {
            pageView.removeFromSuperview()
            pageViews[page] = nil
        }
    }
    func loadVisiblePages() {
        // First, determine which page is currently visible
        let pageWidth = UIScreen.main.bounds.size.width
        let page = Int(floor((headerScrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
        
        // Update the page control
        UIView.animate(withDuration: 0.2, animations: {
            self.indicatorView.frame.origin.x = CGFloat(page) * self.indicatorView.frame.size.width
        })
        
        // Work out which pages you want to load
        let firstPage = page - 1
        let lastPage = page + 1
        
        // Purge anything before the first page
        if firstPage >= 0 {
            for index in 0 ..< firstPage {
                purgePage(index)
            }
        }
        // Load pages in our range
        for index in firstPage...lastPage {
            loadPage(index)
        }
        // Purge anything after the last page
        let tempPage = lastPage + 1
        for index in stride(from: tempPage, to: dataSource.count, by: 1){
            purgePage(index)
        }
    }
    
    func selectRow(_ sender: UIGestureRecognizer) {
        let image = sender.view! as! UIImageView
        
        let browser = SKPhotoBrowser(originImage: image.image!, photos: images, animatedFromView: sender.view!)
        browser.initializePageIndex(sender.view!.tag)
        present(browser, animated: true, completion: {})
    }
    
}

