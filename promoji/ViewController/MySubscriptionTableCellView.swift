//
//  MySubscriptionTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 9/10/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class MySubscriptionTableCellView : UITableViewCell {
    
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgIndicator: UIImageView!
    @IBOutlet weak var lblSubscription: UILabel!
    @IBOutlet weak var viewHolder: UIView!
    
    func buildLayout() {
        iconView.layer.cornerRadius = 4.0
        iconView.layer.masksToBounds = true
        
        viewHolder.layer.cornerRadius = 8.0
        viewHolder.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}