//
//  CoreViewController+Navigation.swift
//  promoji
//
//  Created by Nurul Fikri on 7/27/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

extension CoreViewController {
    
    func toBrandDetail() {
        let storyBoard = self.appDelegate().grabStoryBoard()
        let viewController: BrandDetailViewController? = storyBoard.instantiateViewController(withIdentifier: "BrandDetailViewController") as? BrandDetailViewController
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    func toPromoDetail() {
        let storyBoard = self.appDelegate().grabStoryBoard()
        let viewController: PromoDetailViewController? = storyBoard.instantiateViewController(withIdentifier: "PromoDetailViewController") as? PromoDetailViewController
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    func toPromoGroupDetail() {
        let storyBoard = self.appDelegate().grabStoryBoard()
        let viewController: PromoGroupViewController? = storyBoard.instantiateViewController(withIdentifier: "promoGroupViewController") as? PromoGroupViewController
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    func toPromoLocationDetail() {
        let storyBoard = self.appDelegate().grabStoryBoard()
        let viewController: PromoLocationViewController? = storyBoard.instantiateViewController(withIdentifier: "promoLocationViewController") as? PromoLocationViewController
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    func toPromoCategoryDetail() {
        let storyBoard = self.appDelegate().grabStoryBoard()
        let viewController: PromoCategoryDetailViewController? = storyBoard.instantiateViewController(withIdentifier: "promoCategoryDetailViewController") as? PromoCategoryDetailViewController
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    func toSearchPromo() {
        let storyBoard = self.appDelegate().grabStoryBoard()
        let viewController: SearchPromoViewController? = storyBoard.instantiateViewController(withIdentifier: "searchPromoViewController") as? SearchPromoViewController
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    func toMySubscription() {
        let storyBoard = self.appDelegate().grabStoryBoard()
        let viewController: MySubscriptionViewController? = storyBoard.instantiateViewController(withIdentifier: "mySubscriptionViewController") as? MySubscriptionViewController
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
//    func toInvitationUserByQuestionList(selected_user: NSMutableArray, survey_id:String, isNeedFeedback: Bool, isForFGD: Bool) {
//        let storyBoard = self.appDelegate().grabStoryBoard()
//        let viewController: QuestionListViewController? = storyBoard.instantiateViewControllerWithIdentifier("QuestionListViewController") as? QuestionListViewController
//        viewController?.selectedUser = selected_user
//        viewController?.isNeedFeedback = isNeedFeedback
//        viewController?.survey_id = survey_id
//        viewController?.isForFGD = isForFGD
//        self.navigationController?.pushViewController(viewController!, animated: true)
//    }
//    
//    func toAddClientPage() {
//        let storyBoard = self.appDelegate().grabStoryBoard()
//        let viewController : AddClientViewController!  = storyBoard.instantiateViewControllerWithIdentifier("AddClientViewController") as! AddClientViewController
//        let navigationController: UINavigationController = UINavigationController(rootViewController: viewController)
//        presentViewController(navigationController, animated: true, completion: nil)
//    }
    
}
