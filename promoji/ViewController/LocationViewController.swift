//
//  LocationViewController.swift
//  promoji
//
//  Created by Nurul Fikri on 7/27/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class LocationViewController: CoreViewController, UITableViewDelegate, UITableViewDataSource {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initPopupInfo(UIImage(named: "tabicon-place-normal")!, title: "Discover Around You", description: "See all ongoing promos near you based on your current location or favorite malls.")
        showPopupInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath as NSIndexPath).row == 0 {
            return 64
        } else {
            return 106
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).row == 0 {
            (cell as! MenuLocationNearbyTableCellView).buildLayout()
        } else {
            (cell as! MenuLocationTableCellView).buildLayout()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath as NSIndexPath).row == 0 {
            let cell: MenuLocationNearbyTableCellView? = tableView.dequeueReusableCell(withIdentifier: "menuLocationNearbyTableCellView", for: indexPath) as? MenuLocationNearbyTableCellView
            return cell!
        } else {
            let cell: MenuLocationTableCellView? = tableView.dequeueReusableCell(withIdentifier: "menuLocationTableCellView", for: indexPath) as? MenuLocationTableCellView
            cell?.imageVIew.sd_setImage(with: URL(string: "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSIl3l5HtwoYP_bcbYqheMFeoL7PyQ7xKML351tB9VkBNwVnmS4"))
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        toPromoLocationDetail()
    }
    
}
