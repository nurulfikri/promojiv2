//
//  SearchViewController.swift
//  promoji
//
//  Created by Nurul Fikri on 7/27/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class SearchViewController: CoreViewController, UITableViewDelegate, UITableViewDataSource, CustomSelectUICollectionViewCellDelegate, UISearchBarDelegate {
    @IBOutlet weak var tblView: UITableView!
    
    let slideView = UIView()
    let indicatorHolderView = UIView()
    let indicatorView = UIView()
    let headerBanner = UIView()
    
    var images = [SKPhoto]()
    var timer : Timer?
    
    var headerScrollView = UIScrollView()
    var pageViews: [UIImageView?] = []
    var searchController : UISearchController!
    
    var dataSource: Array<URL> = {
        var array: Array<URL> = []
        
        array.append(URL(string:"http://previews.123rf.com/images/sellingpix/sellingpix1409/sellingpix140900047/31788840-Oktoberfest-festival-de-la-tipograf-a-de-estilo-retro-del-cartel-del-vector-del-dise-o-de-la-vendimi-Foto-de-archivo.jpg")!)
        array.append(URL(string:"https://s-media-cache-ak0.pinimg.com/236x/f7/8c/9a/f78c9a75a989652c2873a0ab2cd66beb.jpg")!)
        array.append(URL(string:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT_wBPiymmFuJyJFaN1h7qMzZP6xyywgZRnZoempwbTTmA1dxCx")!)
        array.append(URL(string:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQML8K1MF0tVJ0xG5ibJIcVXROGOgvuPru4XS592BjyYDQTuReT")!)
        array.append(URL(string:"https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRTAmbJtZpe3eLr6IdESQW-bb6fHO8FZ65R6ZHIE8IM0jVD1XHOy1twpjs")!)
        array.append(URL(string:"https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcT25v0Vu7CrKswR9yWRPrXCO2JgtqhueWDgLXKDLnI8VwBWpPKS")!)
        array.append(URL(string:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQGl3VyvCle0ar5Hoc12CeOMZmXlMnnlioxam43Y9Znm-fO0Nup")!)
        array.append(URL(string:"https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSpL-YbhEZyMxDIYcWAfi4HYgm3mw6IdLyBAB9SR9KO5W-fPWdb")!)
        array.append(URL(string:"https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSvGvw69jDzGVUQN5OA-pCame3dlyDE0szlGtV-QBoCpOye1OkA")!)
        array.append(URL(string:"http://previews.123rf.com/images/sellingpix/sellingpix1409/sellingpix140900047/31788840-Oktoberfest-festival-de-la-tipograf-a-de-estilo-retro-del-cartel-del-vector-del-dise-o-de-la-vendimi-Foto-de-archivo.jpg")!)
        
        return array
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initSearchHeader()
        
        for url in dataSource {
            let photo = SKPhoto.photoWithImageURL(url.absoluteString)
            photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
            images.append(photo)
        }
        
        initHeaderScroll()
        self.title = "Search"
        
        initPopupInfo(UIImage(named: "tabicon-browse-normal")!, title: "Search & Browse Promos", description: "Browse all ongoing promos by collections, categories, popularity or search")
        showPopupInfo()
    }
    
    func initSearchHeader() {
        self.searchController = UISearchController(searchResultsController:  nil)
        
        self.searchController.searchBar.delegate = self

        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = true
        self.navigationItem.titleView = searchController.searchBar
        
        self.definesPresentationContext = false
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        toSearchPromo()
        return false
    }
    
    func updateSearchResultsForSearchController(_ searchController: UISearchController) {
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (indexPath as NSIndexPath).row {
        case 0:
            return 122
        case 1:
            return 260
        case 2:
            return 226
        case 3:
            return 122
        case 4:
            return 260
        case 5:
            return 188 + ((14-2)/2*106)
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch (indexPath as NSIndexPath).row {
        case 0:
            (cell as! BrowseGroupTableCellView).buildLayout()
        case 1:
            (cell as! BrowsePromoTableCellView).buildLayout()
        case 2:
            (cell as! BrowseBrandTableCellView).buildLayout()
        case 3:
            (cell as! BrowseGroupTableCellView).buildLayout()
        case 4:
            (cell as! BrowsePromoTableCellView).buildLayout()
        case 5:
            (cell as! BrowseCategoryTableCellView).buildLayout()
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath as NSIndexPath).row {
        case 0:
            let cell: BrowseGroupTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "browseGroupTableCellView", for: indexPath) as? BrowseGroupTableCellView
            cell?.selectDelegate = self
            return cell!
        case 1:
            let cell: BrowsePromoTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "browsePromoTableCellView", for: indexPath) as? BrowsePromoTableCellView
            cell?.lblTitle.text = "POPULAR PROMOS"
            cell?.selectDelegate = self
            return cell!
        case 2:
            let cell: BrowseBrandTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "browseBrandTableCellView", for: indexPath) as? BrowseBrandTableCellView
            cell?.selectDelegate = self
            cell?.lblTitle.text = "FEATURED BRANDS"
            return cell!
        case 3:
            let cell: BrowseGroupTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "browseGroupTableCellView", for: indexPath) as? BrowseGroupTableCellView
            cell?.selectDelegate = self
            return cell!
        case 4:
            let cell: BrowsePromoTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "browsePromoTableCellView", for: indexPath) as? BrowsePromoTableCellView
            cell?.lblTitle.text = "NEWEST PROMOS"
            cell?.selectDelegate = self
            return cell!
        case 5:
            let cell: BrowseCategoryTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "browseCategoryTableCellView", for: indexPath) as? BrowseCategoryTableCellView
            cell?.selectDelegate = self
            return cell!
        default:
            let cell: BrowseMainTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "browseMainTableCellView", for: indexPath) as? BrowseMainTableCellView
            return cell!
        }
    }
    
    func didSelectCollectionViewCell(_ sender: UITableViewCell, indexPath: IndexPath, info: AnyObject?) {
        if sender.isKind(of: BrowseGroupTableCellView.self) {
            toPromoGroupDetail()
        } else if sender.isKind(of: BrowsePromoTableCellView.self) {
            toPromoDetail()
        } else if sender.isKind(of: BrowseBrandTableCellView.self) {
            toBrandDetail()
        } else if sender.isKind(of: BrowseCategoryTableCellView.self) {
            toPromoCategoryDetail()
        }
    }
    
}

extension SearchViewController: UIScrollViewDelegate {
    func initHeaderScroll() {
        // 3
        for _ in 0..<dataSource.count {
            pageViews.append(nil)
        }
        
        headerBanner.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 176)
        headerBanner.backgroundColor = UIColor.white
        
        headerScrollView.showsHorizontalScrollIndicator = false
        headerScrollView.frame = headerBanner.frame
        headerScrollView.delegate = self
        headerScrollView.isPagingEnabled = true
        headerScrollView.contentSize = CGSize(width: headerBanner.frame.size.width * CGFloat(dataSource.count),
                                              height: 0)
        headerBanner.addSubview(headerScrollView)
        
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(PromoDetailViewController.goNextPage), userInfo: nil, repeats: true)
        
        indicatorHolderView.frame = CGRect(x: 0, y: headerBanner.frame.size.height - 4, width: headerBanner.frame.size.width, height: 4)
        indicatorHolderView.backgroundColor = UIColor(white: 0.0, alpha: 0.4)
        indicatorView.frame = CGRect(x: 0, y: 0, width: indicatorHolderView.frame.width/CGFloat(dataSource.count), height: 4)
        indicatorView.backgroundColor = UIColor(white: 1.0, alpha: 0.75)
        indicatorHolderView.addSubview(indicatorView)
        headerBanner.addSubview(indicatorHolderView)
        
        tblView.tableHeaderView = headerBanner
        
        loadVisiblePages()
    }
    
    
    func goNextPage() {
        if dataSource.count > 1 {
            let pageWidth = UIScreen.main.bounds.size.width
            let page = Int(floor((headerScrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
            
            if page+1 < dataSource.count {
                UIView.animate(withDuration: 0.2, animations: {
                    self.headerScrollView.contentOffset.x = self.headerScrollView.contentOffset.x + pageWidth
                })
                
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.headerScrollView.contentOffset.x = 0
                })
            }
        }
        
    }
    
    // MARK: - ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == headerScrollView {
            loadVisiblePages()
        }
    }
    
    func loadPage(_ page: Int) {
        if page < 0 || page >= dataSource.count {
            // If it's outside the range of what you have to display, then do nothing
            return
        }
        
        // 1
        if pageViews[page] != nil {
            
        } else {
            let screenSize: CGRect = UIScreen.main.bounds
            let screenWidth = screenSize.width
            var frame = headerScrollView.frame
            frame.size.width=screenWidth
            frame.origin.x = screenWidth * CGFloat(page)
            frame.origin.y = 0.0
            
            let newPageView = UIImageView()
            newPageView.sd_setImage(with: dataSource[page])
            newPageView.contentMode = .scaleToFill
            newPageView.clipsToBounds = true
            newPageView.frame = frame
            newPageView.isUserInteractionEnabled = true
            newPageView.tag = page
            let gestureRecognize = UITapGestureRecognizer(target: self, action: #selector(PromoDetailViewController.selectRow(_:)))
            newPageView.addGestureRecognizer(gestureRecognize)
            
            headerScrollView.addSubview(newPageView)
            
            pageViews[page] = newPageView
        }
    }
    func purgePage(_ page: Int) {
        if page < 0 || page >= dataSource.count {
            // If it's outside the range of what you have to display, then do nothing
            return
        }
        
        // Remove a page from the scroll view and reset the container array
        if let pageView = pageViews[page] {
            pageView.removeFromSuperview()
            pageViews[page] = nil
        }
    }
    func loadVisiblePages() {
        // First, determine which page is currently visible
        let pageWidth = UIScreen.main.bounds.size.width
        let page = Int(floor((headerScrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
        
        // Update the page control
        UIView.animate(withDuration: 0.2, animations: {
            self.indicatorView.frame.origin.x = CGFloat(page) * self.indicatorView.frame.size.width
        })
        
        // Work out which pages you want to load
        let firstPage = page - 1
        let lastPage = page + 1
        
        // Purge anything before the first page
        if firstPage >= 0 {
            for index in 0 ..< firstPage {
                purgePage(index)
            }
        }
        // Load pages in our range
        for index in firstPage...lastPage {
            loadPage(index)
        }
        // Purge anything after the last page
        let tempPage = lastPage + 1
        for index in stride(from: tempPage, to: dataSource.count, by: 1){
            purgePage(index)
        }
    }
    
    func selectRow(_ sender: UIGestureRecognizer) {
        let image = sender.view! as! UIImageView
        
        let browser = SKPhotoBrowser(originImage: image.image!, photos: images, animatedFromView: sender.view!)
        browser.initializePageIndex(sender.view!.tag)
        present(browser, animated: true, completion: {})
    }
    
}
