//
//  CoreViewController.swift
//  promoji
//
//  Created by Nurul Fikri on 7/27/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import UIKit
import Foundation


protocol CustomSelectUICollectionViewCellDelegate {
    func didSelectCollectionViewCell(_ sender:UITableViewCell, indexPath:IndexPath, info:AnyObject?)
}


class CoreViewController: UIViewController {
    
    var popupInfo: UIView = UIView()
    let popupInfoHeight:CGFloat = 240
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initNavigationBar()
    }
    
    func appDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func initNavigationBar() {
        if let topItem = self.navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        }
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
//        let ver: NSArray  = UIDevice.current.systemVersion.components(separatedBy: ".")
        let ver = UIDevice.current.systemVersion.components(separatedBy: ".")
        if Int(ver[0])! >= 7 {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 43.0/255.0, green: 179.0/255.0, blue: 58.0/255.0, alpha: 1)
            self.navigationController?.navigationBar.isTranslucent = false
        } else {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.barTintColor = UIColor.orange
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.tintColor = UIColor(red: 43.0/255.0, green: 179.0/255.0, blue: 58.0/255.0, alpha: 1)
            UINavigationBar.appearance().tintColor = UIColor.white
        }
        UINavigationBar.appearance().tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
    func initPopupInfo(_ image: UIImage, title: String, description: String) {
        popupInfo = UIView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-popupInfoHeight, width: UIScreen.main.bounds.width, height: popupInfoHeight))
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.popupInfo.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.6)
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            //always fill the view
            blurEffectView.frame = self.view.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.popupInfo.addSubview(blurEffectView)
        } else {
            self.popupInfo.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.8)
        }
        self.popupInfo.alpha = 0
        self.parent!.view.addSubview(popupInfo)
        
        //Add Image Icon
        let imageHeight: CGFloat = 48
        let imageWidth: CGFloat = 48
        let imageIcon = UIImageView(frame: CGRect(x: (UIScreen.main.bounds.width-imageWidth)/2, y: 28, width: imageWidth, height: imageHeight))
        imageIcon.image = image
        self.popupInfo.addSubview(imageIcon)
        
        //Add Title
        let titleLabel = UILabel(frame: CGRect(x: 16, y: imageIcon.frame.origin.y + imageIcon.frame.size.height + 16, width: UIScreen.main.bounds.width-32, height: 21))
        titleLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 16.0)
        titleLabel.text = title
        titleLabel.textAlignment = NSTextAlignment.center
        titleLabel.textColor = UIColor.white
        self.popupInfo.addSubview(titleLabel)
        
        //Add Description
        let descriptionLabel = UILabel(frame: CGRect(x: 16, y: titleLabel.frame.origin.y + titleLabel.frame.size.height + 8, width: UIScreen.main.bounds.width-32, height: 42))
        descriptionLabel.font = UIFont(name: "HelveticaNeue", size: 16.0)
        descriptionLabel.text = description
        descriptionLabel.numberOfLines = 2
        descriptionLabel.textAlignment = NSTextAlignment.center
        descriptionLabel.textColor = UIColor.white
        self.popupInfo.addSubview(descriptionLabel)
        
        //Add Button
        let buttonHeight: CGFloat = 40
        let buttonWidth: CGFloat = 120
        let buttonOk = UIButton(type: .custom)
        buttonOk.frame = CGRect(x: (UIScreen.main.bounds.width-buttonWidth)/2, y: descriptionLabel.frame.origin.y + descriptionLabel.frame.size.height + 16, width: buttonWidth, height: buttonHeight)
        buttonOk.backgroundColor = UIColor.clear
        buttonOk.layer.borderWidth = 1
        buttonOk.layer.borderColor = UIColor.white.cgColor
        buttonOk.layer.cornerRadius = buttonHeight/2
        buttonOk.layer.masksToBounds = true
        buttonOk.setTitle("OK", for: UIControlState())
        buttonOk.addTarget(self, action: (#selector(CoreViewController.hidePopupInfo)), for: UIControlEvents.touchUpInside)
        self.popupInfo.addSubview(buttonOk)
    }
    
    func showPopupInfo() {
        UIView.animate(withDuration: 0.5, animations: {
            self.popupInfo.alpha = 1
            self.tabBarController?.tabBar.isHidden = true
        })
    }
    
    func hidePopupInfo() {
        UIView.animate(withDuration: 0.5, animations: {
            self.popupInfo.alpha = 0
            self.tabBarController?.tabBar.isHidden = false
        })
    }
    
}
