//
//  BrandDetailViewController.swift
//  promoji
//
//  Created by Nurul Fikri on 8/24/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class BrandDetailViewController: CoreViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    var header : StretchHeader!
    var navigationView = UIView()
    
    let STATE_PROMOS = 0
    let STATE_UPDATES = 1
    let STATE_DETAILS = 2
    
    var states = 0
    
    var arrayLocation = NSMutableArray()
    var arrayBrandsRelated = NSMutableArray()
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.register(UITableViewCell.self, forCellReuseIdentifier: "TableViewCell")
        setupHeaderView()
        
        // NavigationHeader
        let navibarHeight : CGFloat = navigationController!.navigationBar.bounds.height
        let statusbarHeight : CGFloat = UIApplication.shared.statusBarFrame.size.height
        navigationView = UIView()
        navigationView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: navibarHeight + statusbarHeight)
        navigationView.backgroundColor = UIColor(red: 43.0/255.0, green: 179.0/255.0, blue: 58.0/255.0, alpha: 1)
        navigationView.alpha = 0.0
        view.addSubview(navigationView)

        // back button
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 10, y: 20, width: 44, height: 44)
        button.setImage(UIImage(named: "back-white-normal")?.withRenderingMode(.alwaysTemplate), for: UIControlState())
        button.tintColor = UIColor.white
        button.addTarget(self, action: #selector(BrandDetailViewController.leftButtonAction), for: .touchUpInside)
        view.addSubview(button)
        
        arrayLocation.add("Citywalk")
        arrayLocation.add("Grand Indonesia")
        arrayLocation.add("Pacific Place")
        
        arrayBrandsRelated.add("Dunkin Donuts")
        arrayBrandsRelated.add("Mc Donalds")
        arrayBrandsRelated.add("KFC")
        arrayBrandsRelated.add("A&W")
        arrayBrandsRelated.add("Rocky")
        arrayBrandsRelated.add("Ayam Taliwang")
    }
    
    func setupHeaderView() {
        
        let options = StretchHeaderOptions()
        options.position = .fullScreenTop
        
        header = StretchHeader()
        header.stretchHeaderSize(headerSize: CGSize(width: view.frame.size.width, height: (UIScreen.main.bounds.height-64-100)/2),
                                 imageSize: CGSize(width: view.frame.size.width, height: 175),
                                 controller: self,
                                 options: options)
        header.imageView.sd_setImage(with: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT93UPlZDPDn1rfvzXTIA18fgcilrSX_R92NCu7hdDFAr5bsKjC"))
        
        header.backgroundColor = UIColor.orange
        // custom
        let avatarImage = UIImageView()
        avatarImage.frame = CGRect(x: (header.imageView.frame.width-96)/2, y: header.imageView.frame.height - (96/2), width: 96, height: 96)
        avatarImage.sd_setImage(with: URL(string: "https://pbs.twimg.com/profile_images/660822549438205953/3pngzQpf.jpg"))
        avatarImage.layer.cornerRadius = 5.0
        avatarImage.clipsToBounds = true
        avatarImage.contentMode = .scaleAspectFit
        
        avatarImage.layer.shadowOpacity = 0.5
        avatarImage.layer.shadowColor = UIColor(white: 0.0, alpha: 0.5).cgColor
        avatarImage.layer.shadowOffset = CGSize(width: 0,height: 0)
        avatarImage.layer.shadowRadius = 8
        
        header.addSubview(avatarImage)
        
        let brandName = UILabel(frame: CGRect(x: 0, y: avatarImage.frame.origin.y + avatarImage.frame.size.height + 12, width: UIScreen.main.bounds.width, height: 20))
        brandName.text = "SIPENULIS"
        brandName.textAlignment = NSTextAlignment.center
        brandName.textColor = UIColor.white
        brandName.font = UIFont.boldSystemFont(ofSize: 20)
        header.addSubview(brandName)
        
        let brandSubscribers = UILabel(frame: CGRect(x: 0, y: brandName.frame.origin.y + brandName.frame.size.height + 8, width: UIScreen.main.bounds.width, height: 12))
        brandSubscribers.text = "3,234,252 Subscribers"
        brandSubscribers.textAlignment = NSTextAlignment.center
        brandSubscribers.textColor = UIColor.white
        brandSubscribers.font = UIFont.systemFont(ofSize: 12)
        header.addSubview(brandSubscribers)
        
        tblView.tableHeaderView = header
    }
    
    // MARK: - Selector
    func leftButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        header.updateScrollViewOffset(scrollView)
        
        // NavigationHeader alpha update
        let offset : CGFloat = scrollView.contentOffset.y
        if (offset > 50) {
            let alpha : CGFloat = min(CGFloat(1), CGFloat(1) - (CGFloat(50) + (navigationView.frame.height) - offset) / (navigationView.frame.height))
            navigationView.alpha = CGFloat(alpha)
            
        } else {
            navigationView.alpha = 0.0;
        }
    }
    
}

extension BrandDetailViewController {
    
    // MARK: - Table view data source
    @objc(numberOfSectionsInTableView:)func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let navibarHeight : CGFloat = navigationController!.navigationBar.bounds.height
            let statusbarHeight : CGFloat = UIApplication.shared.statusBarFrame.size.height
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 56 + navibarHeight + statusbarHeight))
            view.backgroundColor = UIColor.orange
            
            let viewTop = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: navibarHeight + statusbarHeight))
            viewTop.backgroundColor = UIColor.clear
            
            let btnSubscribe = UIButton(frame: CGRect(x: (UIScreen.main.bounds.width - 160)/2, y: 8, width: 160, height: viewTop.frame.size.height - 16))
            btnSubscribe.layer.cornerRadius = btnSubscribe.frame.size.height/2
            btnSubscribe.clipsToBounds = true
            btnSubscribe.backgroundColor = UIColor.white
            btnSubscribe.setTitle("Subscribe", for: UIControlState())
            btnSubscribe.setTitleColor(UIColor.orange, for: UIControlState())
            viewTop.addSubview(btnSubscribe)
            view.addSubview(viewTop)
            
            let viewBottom = UIView(frame: CGRect(x: 0, y: viewTop.frame.size.height, width: UIScreen.main.bounds.width, height: 56))
            viewBottom.backgroundColor = UIColor(white: 0.0, alpha: 0.4)
            
            let containerWidth = viewBottom.frame.size.width - 32
            let btnWidth = containerWidth/3
            let btnHeight = viewBottom.frame.size.height
            
            let btnPromosX: CGFloat = 16
            let btnUpdatesX: CGFloat = 16 + btnWidth
            let btnDetailsX: CGFloat = 16 + (btnWidth * 2)
            
            let btnPromos = UIButton(frame: CGRect(x: btnPromosX, y: 0, width: btnWidth, height: btnHeight))
            btnPromos.setTitle("Promos", for: UIControlState())
            btnPromos.setTitleColor(UIColor.white, for: UIControlState())
            btnPromos.addTarget(self, action: #selector(BrandDetailViewController.statePromoClick), for: UIControlEvents.touchUpInside)
            viewBottom.addSubview(btnPromos)
            
            let btnUpdates = UIButton(frame: CGRect(x: btnUpdatesX, y: 0, width: btnWidth, height: btnHeight))
            btnUpdates.setTitle("Updates", for: UIControlState())
            btnUpdates.setTitleColor(UIColor.white, for: UIControlState())
            btnUpdates.addTarget(self, action: #selector(BrandDetailViewController.stateUpdateClick), for: UIControlEvents.touchUpInside)
            viewBottom.addSubview(btnUpdates)
            
            let btnDetails = UIButton(frame: CGRect(x: btnDetailsX, y: 0, width: btnWidth, height: btnHeight))
            btnDetails.setTitle("Details", for: UIControlState())
            btnDetails.setTitleColor(UIColor.white, for: UIControlState())
            btnDetails.addTarget(self, action: #selector(BrandDetailViewController.stateDetailClick), for: UIControlEvents.touchUpInside)
            viewBottom.addSubview(btnDetails)
            
            let viewIndicator = UIView(frame: CGRect(x: 0, y: btnHeight - 3, width: btnWidth, height: 3))
            viewIndicator.backgroundColor = UIColor(white: 1.0, alpha: 0.6)
            
            if states == STATE_PROMOS {
                viewIndicator.frame.origin.x = btnPromosX
            } else if states == STATE_DETAILS {
                viewIndicator.frame.origin.x = btnDetailsX
            } else {
                viewIndicator.frame.origin.x = btnUpdatesX
            }
            viewBottom.addSubview(viewIndicator)
            
            view.addSubview(viewBottom)
            
            return view
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            
            let navibarHeight : CGFloat = navigationController!.navigationBar.bounds.height
            let statusbarHeight : CGFloat = UIApplication.shared.statusBarFrame.size.height
            return 56 + navibarHeight + statusbarHeight
        }
        return 0
    }
    
    @objc(tableView:heightForRowAtIndexPath:) func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if states == STATE_PROMOS {
            if (indexPath as NSIndexPath).row % 2 == 0 {
                return 465
            } else {
                return 130
            }
        } else if states == STATE_UPDATES {
            if (indexPath as NSIndexPath).row % 4 == 0 {
                return 32
            } else if (indexPath as NSIndexPath).row % 3 == 0 {
                return 347
            } else {
                let height = "lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ".heightWithConstrainedWidth(276, font: UIFont.systemFont(ofSize: 14))
                return 34 + height
            }
        } else if states == STATE_DETAILS {
            if (indexPath as NSIndexPath).row == 0 {
                return 104
            } else if (indexPath as NSIndexPath).row == 1 {
                return 77
            } else if (indexPath as NSIndexPath).row == 2 {
                return 108
            } else if (indexPath as NSIndexPath).row == 3 {
                return 196 + 42 + (CGFloat(arrayLocation.count > 5 ? 5 : arrayLocation.count) * 46) + 16
            } else if (indexPath as NSIndexPath).row == 4 {
                return 52 + 42 + (CGFloat(arrayBrandsRelated.count > 5 ? 5 : arrayBrandsRelated.count) * 72) + 16
            } else {
                return 0
            }
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if states == STATE_DETAILS {
            return 5
        }
        return 20
    }
    
    @objc(tableView:willDisplayCell:forRowAtIndexPath:) func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch states {
        case STATE_PROMOS:
            if (indexPath as NSIndexPath).row % 2 == 0 {
                (cell as! BigPromoCellView).buildLayout()
            } else {
                
                (cell as! SmallPromoCellView).buildLayout()
            }
        case STATE_UPDATES:
            if (indexPath as NSIndexPath).row % 4 == 0 {
                (cell as! BrandUpdatesDateTableCellView).buildLayout()
            } else if (indexPath as NSIndexPath).row % 3 == 0 {
                (cell as! BrandUpdatesImageTableCellView).buildLayout()
            } else {
                (cell as! BrandUpdatesTextTableCellView).buildLayout()
            }
        case STATE_DETAILS:
            if (indexPath as NSIndexPath).row == 0 {
                (cell as! BrandDetailsInformationTableCellView).buildLayout()
            } else if (indexPath as NSIndexPath).row == 1 {
                (cell as! BrandDetailsAboutTableCellView).buildLayout()
            } else if (indexPath as NSIndexPath).row == 2 {
                (cell as! BrandDetailsConnectTableCellView).buildLayout()
            } else if (indexPath as NSIndexPath).row == 3 {
                (cell as! BrandDetailsLocationTableViewCell).buildLayout()
            } else if (indexPath as NSIndexPath).row == 4 {
                (cell as! BrandDetailsRelatedTableCellView).buildLayout()
            }
        
        default:
           return
        }
    }
    
    @objc(tableView:cellForRowAtIndexPath:) func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch states {
        case STATE_PROMOS:
            if (indexPath as NSIndexPath).row % 2 == 0 {
                let cell: BigPromoCellView? = self.tblView.dequeueReusableCell(withIdentifier: "bigPromoCellView", for: indexPath) as? BigPromoCellView
                cell?.bigPicture.sd_setImage(with: URL(string: "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSQHv6Wvso7emdD05ulmXJNo7sVs3rIoSyqiTlnA_9YwoXns_pi"))
                cell?.merchantImage.sd_setImage(with: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxMPTIVLiNwGonPvjeOVpR5fWnLCvfeCqSsrAhAXsbs0r3Wg1JGw"))
                cell?.merchantPromo = "Sipenulis"
                cell?.promoterString = "Fikri"
                return cell!
            } else {
                let cell: SmallPromoCellView? = self.tblView.dequeueReusableCell(withIdentifier: "smallPromoCellView", for: indexPath) as? SmallPromoCellView
                cell?.merchantImage.sd_setImage(with: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxMPTIVLiNwGonPvjeOVpR5fWnLCvfeCqSsrAhAXsbs0r3Wg1JGw"))
                cell?.merchantPromo = "Sipenulis"
                cell?.promoterString = "Fikri"
                return cell!
            }
        case STATE_UPDATES:
            if (indexPath as NSIndexPath).row % 4 == 0 {
                let cell: BrandUpdatesDateTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "brandUpdatesDateTableCellView", for: indexPath) as? BrandUpdatesDateTableCellView
                cell?.btnDate.setTitle("Tomorrow", for: UIControlState())
                return cell!
            } else if (indexPath as NSIndexPath).row % 3 == 0 {
                let cell: BrandUpdatesImageTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "brandUpdatesImageTableCellView", for: indexPath) as? BrandUpdatesImageTableCellView
                cell?.imageContent.sd_setImage(with: URL(string: "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSQHv6Wvso7emdD05ulmXJNo7sVs3rIoSyqiTlnA_9YwoXns_pi"))
                cell?.imageBrand.sd_setImage(with: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxMPTIVLiNwGonPvjeOVpR5fWnLCvfeCqSsrAhAXsbs0r3Wg1JGw"))
                cell?.lblAccountName.text = "@sipenulis"
                cell?.lblTime.text = ". 2h"
                return cell!
            } else {
                let cell: BrandUpdatesTextTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "brandUpdatesTextTableCellView", for: indexPath) as? BrandUpdatesTextTableCellView
                cell?.imageBrand.sd_setImage(with: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxMPTIVLiNwGonPvjeOVpR5fWnLCvfeCqSsrAhAXsbs0r3Wg1JGw"))
                cell?.lblContent.text = "lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum "
                cell?.lblAccountName.text = "@sipenulis"
                cell?.lblTime.text = ". 2h"
                return cell!
            }
        case STATE_DETAILS:
            if (indexPath as NSIndexPath).row == 0 {
                let cell: BrandDetailsInformationTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "brandDetailsInformationTableCellView", for: indexPath) as? BrandDetailsInformationTableCellView
                return cell!
            } else if (indexPath as NSIndexPath).row == 1 {
                let cell: BrandDetailsAboutTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "brandDetailsAboutTableCellView", for: indexPath) as? BrandDetailsAboutTableCellView
                return cell!
            } else if (indexPath as NSIndexPath).row == 2 {
                let cell: BrandDetailsConnectTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "brandDetailsConnectTableCellView", for: indexPath) as? BrandDetailsConnectTableCellView
                return cell!
            } else if (indexPath as NSIndexPath).row == 3 {
                let cell: BrandDetailsLocationTableViewCell? = self.tblView.dequeueReusableCell(withIdentifier: "brandDetailsLocationTableViewCell", for: indexPath) as? BrandDetailsLocationTableViewCell
                cell?.arrayLocation = arrayLocation
                return cell!
            } else if (indexPath as NSIndexPath).row == 4 {
                let cell: BrandDetailsRelatedTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "brandDetailsRelatedTableCellView", for: indexPath) as? BrandDetailsRelatedTableCellView
                cell?.arrayRelated = arrayBrandsRelated
                return cell!
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath)
                cell.textLabel?.text = "index -- \((indexPath as NSIndexPath).row)"
                return cell
            }
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath)
            cell.textLabel?.text = "index -- \((indexPath as NSIndexPath).row)"
            return cell
        }
    }
    
    func stateUpdateClick() {
        states = STATE_UPDATES
        self.tblView.reloadData()
    }
    
    func statePromoClick() {
        states = STATE_PROMOS
        self.tblView.reloadData()
    }
    
    func stateDetailClick() {
        states = STATE_DETAILS
        self.tblView.reloadData()
    }
    
}
