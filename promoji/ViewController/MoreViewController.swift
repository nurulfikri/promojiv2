//
//  MoreViewController.swift
//  promoji
//
//  Created by Nurul Fikri on 7/27/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class MoreViewController: CoreViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        initPopupInfo(UIImage(named: "tabicon-more-normal")!, title: "Personalize Everything", description: "Manage and control everything personal here, in one place.")
        showPopupInfo()
        
        if #available(iOS 9, *) {
            tblView.cellLayoutMarginsFollowReadableWidth = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 5
        } else if section == 1{
            return 2
        } else if section == 2{
            return 4
        } else if section == 3{
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as! SettingTableCellView).buildLayout()
        
        if (cell.responds(to: #selector(getter: UIView.tintColor))){
            if (tableView == self.tblView) {
                let cornerRadius : CGFloat = 12.0
                cell.backgroundColor = UIColor.clear
                let layer: CAShapeLayer = CAShapeLayer()
                let pathRef:CGMutablePath = CGMutablePath()
                let bounds: CGRect = cell.bounds.insetBy(dx: 8, dy: 0)
                var addLine: Bool = false
                
                if ((indexPath as NSIndexPath).row == 0 && (indexPath as NSIndexPath).row == tableView.numberOfRows(inSection: (indexPath as NSIndexPath).section)-1) {
                    pathRef.__addRoundedRect(transform: nil, rect: bounds, cornerWidth: cornerRadius, cornerHeight: cornerRadius)
                } else if ((indexPath as NSIndexPath).row == 0) {
                    
                    pathRef.move(to: CGPoint(x: bounds.minX, y: bounds.maxY))
                    pathRef.addArc(tangent1End: CGPoint(x: bounds.minX, y: bounds.minY), tangent2End: CGPoint(x: bounds.midX, y: bounds.minY), radius: cornerRadius)
                    pathRef.addArc(tangent1End: CGPoint(x: bounds.maxX, y: bounds.minY), tangent2End: CGPoint(x: bounds.maxX, y: bounds.midY), radius: cornerRadius)
                    pathRef.addLine(to: CGPoint(x: bounds.maxX, y: bounds.maxY))
                    addLine = true
                } else if ((indexPath as NSIndexPath).row == tableView.numberOfRows(inSection: (indexPath as NSIndexPath).section)-1) {
                    pathRef.move(to: CGPoint(x: bounds.minX, y: bounds.minY))
                    pathRef.addArc(tangent1End: CGPoint(x: bounds.minX, y: bounds.maxY), tangent2End: CGPoint(x: bounds.midX, y: bounds.maxY), radius: cornerRadius)
                    pathRef.addArc(tangent1End: CGPoint(x: bounds.maxX, y: bounds.maxY), tangent2End: CGPoint(x: bounds.maxX, y: bounds.midY), radius: cornerRadius)
                    pathRef.addLine(to: CGPoint(x: bounds.maxX, y: bounds.minY))
                } else {
                    pathRef.addRect(bounds)
                    addLine = true
                }
                
                layer.path = pathRef
                layer.fillColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 0.8).cgColor
                
                if (addLine == true) {
                    let lineLayer: CALayer = CALayer()
                    let lineHeight: CGFloat = (1.0 / UIScreen.main.scale)
                    lineLayer.frame = CGRect(x: bounds.minX+52, y: bounds.size.height-lineHeight, width: bounds.size.width-52, height: lineHeight)
                    lineLayer.backgroundColor = tableView.separatorColor!.cgColor
                    layer.addSublayer(lineLayer)
                }
                let testView: UIView = UIView(frame: bounds)
                testView.layer.insertSublayer(layer, at: 0)
                testView.backgroundColor = UIColor.clear
                cell.backgroundView = testView
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SettingTableCellView = self.tblView.dequeueReusableCell(withIdentifier: "settingTableCellView", for: indexPath) as! SettingTableCellView
        cell.iconView.sd_setImage(with: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxMPTIVLiNwGonPvjeOVpR5fWnLCvfeCqSsrAhAXsbs0r3Wg1JGw"))
        
        if (indexPath as NSIndexPath).section == 0 {
            switch (indexPath as NSIndexPath).row {
            case 0:
                cell.lblTitle.text = "Manage Account"
                cell.lblCount.text = ""
            case 1:
                cell.lblTitle.text = "Mojied Promos"
                cell.lblCount.text = "512"
            case 2:
                cell.lblTitle.text = "My Subscriptions"
                cell.lblCount.text = "96"
            case 3:
                cell.lblTitle.text = "Notification Settings"
                cell.lblCount.text = ""
            case 4:
                cell.lblTitle.text = "Recently Viewed"
                cell.lblCount.text = ""
            default:
                cell.lblTitle.text = ""
                cell.lblCount.text = ""
            }
        } else if (indexPath as NSIndexPath).section == 1 {
            switch (indexPath as NSIndexPath).row {
            case 0:
                cell.lblTitle.text = "Invite Friends"
                cell.lblCount.text = ""
            case 1:
                cell.lblTitle.text = "Rate Us"
                cell.lblCount.text = ""
            default:
                cell.lblTitle.text = ""
                cell.lblCount.text = ""
            }
        } else if (indexPath as NSIndexPath).section == 2 {
            switch (indexPath as NSIndexPath).row {
            case 0:
                cell.lblTitle.text = "Terms of Use"
                cell.lblCount.text = ""
            case 1:
                cell.lblTitle.text = "Privacy Policy"
                cell.lblCount.text = ""
            case 2:
                cell.lblTitle.text = "Contact Us"
                cell.lblCount.text = ""
            case 3:
                cell.lblTitle.text = "About"
                cell.lblCount.text = ""
            default:
                cell.lblTitle.text = ""
                cell.lblCount.text = ""
            }
        } else {
            cell.lblTitle.text = "Logout"
            cell.lblCount.text = ""
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).section == 0 {
            switch (indexPath as NSIndexPath).row {
            case 0:
                break
            case 1:
                break
            case 2:
                toMySubscription()
            case 3:
                break
            case 4:
                break
            default:
                break
            }
        } else if (indexPath as NSIndexPath).section == 1 {
            switch (indexPath as NSIndexPath).row {
            case 0:
                break
            case 1:
                break
            default:
                break
            }
        } else if (indexPath as NSIndexPath).section == 2 {
            switch (indexPath as NSIndexPath).row {
            case 0:
                break
            case 1:
                break
            case 2:
                break
            case 3:
                break
            default:
                break
            }
        } else {
            
        }
    }

}
