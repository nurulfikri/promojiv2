//
//  APIResponse.swift
//  promoji
//
//  Created by Nurul Fikri on 7/27/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class APIResponse: NSObject {
    
    var errorCode = 0
    var errorDescription: String?
    var responseDataDictionary: NSDictionary?
    var responseDataArray: NSArray?
    var responseDictionary: NSDictionary?
    var requestParamDictionary: NSDictionary?
    var hasInternetConnection: Bool?
    var apiError: NSError?
    
    override init() {
        super.init()
    }
}