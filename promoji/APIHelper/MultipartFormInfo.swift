//
//  MultipartFormInfo.swift
//  promoji
//
//  Created by Nurul Fikri on 7/27/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class MultipartFormInfo: NSObject {
    
    let MULTIPARTFORMINFO_MIME_TYPE_PNG = "image/png"
    let MULTIPARTFORMINFO_MIME_TYPE_JPEG = "image/jpeg"
    var data: Data?
    var name: String?
    var filename: String?
    var mimeType: String?
    
    override init() {
        super.init()
    }
}
