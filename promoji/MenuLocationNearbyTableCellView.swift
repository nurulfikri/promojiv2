//
//  MenuLocationNearbyTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/23/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class MenuLocationNearbyTableCellView: UITableViewCell {
    @IBOutlet weak var btnNearby: UIButton!
    let gradientLayer = CAGradientLayer()
    
    func buildLayout() {
        self.btnNearby.layer.cornerRadius = btnNearby.frame.size.height/2
        self.btnNearby.layer.masksToBounds = true
        self.btnNearby.setNeedsLayout()
        self.btnNearby.setNeedsUpdateConstraints()
    }
}