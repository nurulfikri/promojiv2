//
//  PromoCaptionTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/30/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class PromoCaptionTableCellView: UITableViewCell {
    @IBOutlet weak var btnMoji: UIButton!
    @IBOutlet weak var btnRemind: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    func buildLayout() {
        btnMoji.layer.cornerRadius = btnMoji.frame.size.height/2
        btnMoji.layer.masksToBounds = true
        
    }
    
}