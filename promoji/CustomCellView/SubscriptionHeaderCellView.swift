//
//  SubscriptionHeaderCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 7/28/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class SubscriptionHeaderCellView: UICollectionViewCell {
    @IBOutlet weak var imageBrand: UIImageView!
    
    override func prepareForReuse() {
        
        self.imageBrand.layer.cornerRadius = 8.0
        self.imageBrand.layer.masksToBounds = true
        self.imageBrand.setNeedsLayout()
        self.imageBrand.setNeedsUpdateConstraints()
    }
    
}