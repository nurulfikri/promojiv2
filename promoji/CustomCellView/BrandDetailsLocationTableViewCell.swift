//
//  BrandDetailsLocationTableViewCell.swift
//  promoji
//
//  Created by Nurul Fikri on 8/28/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class BrandDetailsLocationTableViewCell : UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    var arrayLocation = NSMutableArray()
    
    func buildLayout() {
        tblView.layer.cornerRadius = 8.0
        tblView.layer.masksToBounds = true
        tblView.delegate = self
        tblView.dataSource = self
        tblView.reloadData()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 + (arrayLocation.count > 5 ? 5 : arrayLocation.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath as NSIndexPath).row == 0 {
            return 196
        } else if (indexPath as NSIndexPath).row == (arrayLocation.count > 5 ? 5 : arrayLocation.count) + 1 {
            return 42
        } else {
            return 46
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).row == 0 {
            (cell as! BrandDetailsLocationHeaderTableCellView).buildLayout()
        } else if (indexPath as NSIndexPath).row == (arrayLocation.count > 5 ? 5 : arrayLocation.count) + 1 {
            (cell as! BrandDetailsFooterViewAllTableCellView).buildLayout()
        } else {
            (cell as! BrandDetailsLocationPointTableCellView).buildLayout()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath as NSIndexPath).row == 0 {
            let cell: BrandDetailsLocationHeaderTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "brandDetailsLocationHeaderTableCellView", for: indexPath) as? BrandDetailsLocationHeaderTableCellView
            return cell!
        } else if (indexPath as NSIndexPath).row == (arrayLocation.count > 5 ? 5 : arrayLocation.count) + 1 {
            let cell: BrandDetailsFooterViewAllTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "brandDetailsFooterViewAllTableCellView", for: indexPath) as? BrandDetailsFooterViewAllTableCellView
            return cell!
        } else {
            let cell: BrandDetailsLocationPointTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "brandDetailsLocationPointTableCellView", for: indexPath) as? BrandDetailsLocationPointTableCellView
            cell?.lblLocation.text = arrayLocation.object(at: (indexPath as NSIndexPath).row - 1) as? String
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    
}
