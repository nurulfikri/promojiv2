//
//  BrandUpdatesDateTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/28/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class BrandUpdatesDateTableCellView : UITableViewCell {
    
    @IBOutlet weak var btnDate: UIButton!
    
    func buildLayout() {
        btnDate.layer.cornerRadius = btnDate.frame.size.height/2
        btnDate.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}