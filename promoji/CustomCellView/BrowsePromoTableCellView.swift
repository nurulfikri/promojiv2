//
//  BrowsePromoTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/22/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class BrowsePromoTableCellView: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnViewAll: UIButton!
    let gradientLayer = CAGradientLayer()
    
    var selectDelegate: CustomSelectUICollectionViewCellDelegate?
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (selectDelegate != nil) {
            selectDelegate?.didSelectCollectionViewCell(self, indexPath: indexPath, info: nil)
        }
    }
    
    func buildLayout() {
        gradientLayer.removeFromSuperlayer()
        gradientLayer.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        gradientLayer.colors = [UIColor.white.cgColor,
                                UIColor(white: 1.0, alpha: 0.5).cgColor]
        
        self.contentView.layer.insertSublayer(gradientLayer, at: 0)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        (cell as! BrowsePromosCellView).buildLayout()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: BrowsePromosCellView = collectionView.dequeueReusableCell(withReuseIdentifier: "browsePromosCellView", for: indexPath) as! BrowsePromosCellView
        cell.imageView.sd_setImage(with: URL(string: "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSQHv6Wvso7emdD05ulmXJNo7sVs3rIoSyqiTlnA_9YwoXns_pi"))
        cell.lblBrand.text = "Sipenulis"
        cell.lblPromos.text = "Discount Buku Besar-Besaran!!"
        
        return cell
    }
    
    
}
