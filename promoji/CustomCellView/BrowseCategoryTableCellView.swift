//
//  BrowseCategoryTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/22/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class BrowseCategoryTableCellView: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var selectDelegate: CustomSelectUICollectionViewCellDelegate?
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (selectDelegate != nil) {
            selectDelegate?.didSelectCollectionViewCell(self, indexPath: indexPath, info: nil)
        }
    }
    
    func buildLayout() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 14
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        (cell as! BrowseCategoryPromosCellView).buildLayout()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: BrowseCategoryPromosCellView = collectionView.dequeueReusableCell(withReuseIdentifier: "browseCategoryPromosCellView", for: indexPath) as! BrowseCategoryPromosCellView
        cell.imageView.sd_setImage(with: URL(string: "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSQHv6Wvso7emdD05ulmXJNo7sVs3rIoSyqiTlnA_9YwoXns_pi"))
        cell.lblCategory.text = "Book"
        
        return cell
    }
    
}
