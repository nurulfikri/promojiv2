//
//  BrandDetailsRelatedTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/28/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class BrandDetailsRelatedTableCellView : UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    var arrayRelated = NSMutableArray()
    
    func buildLayout() {
        tblView.layer.cornerRadius = 8.0
        tblView.layer.masksToBounds = true
        tblView.delegate = self
        tblView.dataSource = self
        tblView.reloadData()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 + (arrayRelated.count > 5 ? 5 : arrayRelated.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath as NSIndexPath).row == 0 {
            return 54
        } else if (indexPath as NSIndexPath).row == (arrayRelated.count > 5 ? 5 : arrayRelated.count) + 1 {
            return 42
        } else {
            return 72
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath as NSIndexPath).row == 0 {
            let cell: BrandDetailsRelatedHeaderTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "brandDetailsRelatedHeaderTableCellView", for: indexPath) as? BrandDetailsRelatedHeaderTableCellView
            return cell!
        } else if (indexPath as NSIndexPath).row == (arrayRelated.count > 5 ? 5 : arrayRelated.count) + 1 {
            let cell: BrandDetailsFooterViewAllTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "brandDetailsFooterViewAllTableCellView", for: indexPath) as? BrandDetailsFooterViewAllTableCellView
            return cell!
        } else {
            let cell: BrandDetailsRelatedBrandTableCellView? = self.tblView.dequeueReusableCell(withIdentifier: "brandDetailsRelatedBrandTableCellView", for: indexPath) as? BrandDetailsRelatedBrandTableCellView
            cell?.lblBrand.text = arrayRelated.object(at: (indexPath as NSIndexPath).row - 1) as! String
            cell?.imageBrand.sd_setImage(with: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxMPTIVLiNwGonPvjeOVpR5fWnLCvfeCqSsrAhAXsbs0r3Wg1JGw"))
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    
}
