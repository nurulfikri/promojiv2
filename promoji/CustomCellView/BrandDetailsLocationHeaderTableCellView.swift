//
//  BrandDetailsLocationHeaderTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/28/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import CoreLocation

class BrandDetailsLocationHeaderTableCellView: UITableViewCell, MKMapViewDelegate, CLLocationManagerDelegate {
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    
    func buildLayout() {
        self.mapView.delegate = self
        
        let cityWalkLocation = CLLocationCoordinate2DMake(-6.2091, 106.8183)
        // Drop a pin
        let cityWalkPIN = MKPointAnnotation()
        cityWalkPIN.coordinate = cityWalkLocation
        cityWalkPIN.title = "Citywalk"
        
        mapView.addAnnotation(cityWalkPIN)
        
        let GILocation = CLLocationCoordinate2DMake(-6.1472, 106.8361)
        // Drop a pin
        let GIPin = MKPointAnnotation()
        GIPin.coordinate = GILocation
        GIPin.title = "Grand Indonesia"
        
        mapView.addAnnotation(GIPin)
        
        let pfLocation = CLLocationCoordinate2DMake(-6.2246, 106.8099)
        // Drop a pin
        let pfPIN = MKPointAnnotation()
        pfPIN.coordinate = pfLocation
        pfPIN.title = "Pacific Place"
        
        mapView.addAnnotation(pfPIN)
        
        let region = self.mapView.regionFromAnnotations(self.mapView.annotations)
        self.mapView.setRegion(region, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation is MKUserLocation) {
            //if annotation is not an MKPointAnnotation (eg. MKUserLocation),
            //return nil so map draws default view for it (eg. blue dot)...
            return nil
        }
        
        let reuseId = "mapIdentifier"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView!.image = UIImage(named:"pin-map-small")
            anView!.canShowCallout = true
            anView!.isEnabled = true
        }
        else {
            //we are re-using a view, update its annotation reference...
            anView!.annotation = annotation
        }
        
        return anView
    }
}
