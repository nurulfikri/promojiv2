//
//  PromoDetailsTermConditionTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/30/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class PromoDetailsTermConditionTableCellView : UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var txtContent: UITextView!
    
    func buildLayout() {
        bgView.layer.cornerRadius = 8.0
        bgView.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}