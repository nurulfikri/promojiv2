//
//  BrowseGroupTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/22/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class BrowseGroupTableCellView: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var collectionView: UICollectionView!
    let gradientLayer = CAGradientLayer()
    
    var selectDelegate: CustomSelectUICollectionViewCellDelegate?
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (selectDelegate != nil) {
            selectDelegate?.didSelectCollectionViewCell(self, indexPath: indexPath, info: nil)
        }
    }
    
    func buildLayout() {
        gradientLayer.removeFromSuperlayer()
        gradientLayer.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        gradientLayer.colors = [UIColor.white.cgColor,
                        UIColor(white: 1.0, alpha: 0.5).cgColor]
        
        self.contentView.layer.insertSublayer(gradientLayer, at: 0)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        (cell as! BrowseGroupPromosCellView).buildLayout()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: BrowseGroupPromosCellView = collectionView.dequeueReusableCell(withReuseIdentifier: "browseGroupPromosCellView", for: indexPath) as! BrowseGroupPromosCellView
        cell.imageView.sd_setImage(with: URL(string: "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSQHv6Wvso7emdD05ulmXJNo7sVs3rIoSyqiTlnA_9YwoXns_pi"))
        
        return cell
    }
    
}
