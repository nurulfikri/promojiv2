//
//  SubscribeHomeCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/2/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class SubscribeHomeCellView: UITableViewCell {
    @IBOutlet weak var subscribtionCollectionView: UICollectionView!
    
    @IBOutlet weak var btnViewAll: UIButton!
}