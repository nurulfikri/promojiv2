//
//  BrandDetailsRelatedBrandTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/28/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class BrandDetailsRelatedBrandTableCellView: UITableViewCell {
    @IBOutlet weak var imageBrand: UIImageView!
    @IBOutlet weak var lblBrand: UILabel!
    @IBOutlet weak var lblTotalSubscriber: UILabel!
    @IBOutlet weak var imageStatus: UIImageView!
    @IBOutlet weak var lblSubscribeStatus: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    func buildLayout() {
        
    }
    
}