//
//  BrowsePromosCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/22/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class BrowsePromosCellView : UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblPromos: UILabel!
    @IBOutlet weak var lblBrand: UILabel!
    
    func buildLayout() {
        let rectShape = CAShapeLayer()
        rectShape.frame = self.imageView.bounds
        rectShape.path = UIBezierPath(roundedRect: self.imageView.bounds, byRoundingCorners: [.topRight, .topLeft], cornerRadii: CGSize(width: 8.0, height: 8.0)).cgPath
        self.imageView.layer.backgroundColor = UIColor.green.cgColor
        self.imageView.layer.mask = rectShape
        
        self.layer.cornerRadius = 8.0
        self.layer.masksToBounds = true
        self.setNeedsLayout()
        self.setNeedsUpdateConstraints()
        self.imageView.setNeedsLayout()
        self.imageView.setNeedsUpdateConstraints()
    }
}
