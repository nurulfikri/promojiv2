//
//  BigPromoCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 7/27/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class BigPromoCellView: UITableViewCell {
    
    @IBOutlet weak var bigPicture: UIImageView!
    @IBOutlet weak var merchantImage: UIImageView!
    @IBOutlet weak var promoName: UILabel!
    @IBOutlet weak var descriptionHolder: UIView!
    @IBOutlet weak var detailHolder: UIView!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var holderBottom: UIView!
    
    var merchantPromo = ""
    var promoterString = ""
    var merchantSelector: Selector?
    var promoSelector: Selector?
    
    func buildLayout() {
        self.bigPicture.frame = CGRect(x: 8, y: 8, width: UIScreen.main.bounds.width - 16, height: UIScreen.main.bounds.width - 16)
        self.holderBottom.frame = CGRect(x: 8, y: self.bigPicture.frame.origin.y + self.bigPicture.frame.size.height, width: self.bigPicture.frame.size.width, height: 120)
        
        self.bigPicture.layoutIfNeeded()
        self.holderBottom.layoutIfNeeded()
        self.bigPicture.setNeedsUpdateConstraints()
        self.holderBottom.setNeedsUpdateConstraints()
        
        let rectShape = CAShapeLayer()
        rectShape.frame = self.bigPicture.bounds
        rectShape.path = UIBezierPath(roundedRect: self.bigPicture.bounds, byRoundingCorners: [.topRight, .topLeft], cornerRadii: CGSize(width: 8.0, height: 8.0)).cgPath
        self.bigPicture.layer.backgroundColor = UIColor.white.cgColor
        self.bigPicture.layer.mask = rectShape
        self.bigPicture.layer.masksToBounds = true
        
//        holderBottom.layer.cornerRadius = 8.0
//        holderBottom.layer.masksToBounds = true
        
        let maskPath = UIBezierPath(roundedRect: holderBottom.bounds,
                                    byRoundingCorners: [.bottomLeft, .bottomRight],
                                    cornerRadii: CGSize(width: 8.0, height: 8.0))
        let shape = CAShapeLayer()
        shape.frame = self.holderBottom.bounds
        shape.path = maskPath.cgPath
        holderBottom.layer.backgroundColor = UIColor.white.cgColor
        holderBottom.layer.mask = shape
        holderBottom.layer.masksToBounds = true

        
//        holderBottom.round([UIRectCorner.BottomRight,UIRectCorner.BottomLeft], radius: 8.0, borderColor: UIColor.clearColor(), borderWidth: 8.0)
        
        UIView().buildTextClickable("by #\(merchantPromo)# with #\(promoterString)", linkString: [merchantPromo:"", promoterString:""], font: UIFont.systemFont(ofSize: 14), textColor: UIColor.darkGray, textClickableColor: UIColor.blue, holderView: descriptionHolder)
        
        UIView().buildTextClickable("Valid until 26 Dec 2016", linkString: ["":""], font: UIFont.systemFont(ofSize: 14), textColor: UIColor.darkGray, textClickableColor: UIColor.blue, holderView: detailHolder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        buildLayout()
    }
    
}
