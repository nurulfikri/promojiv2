//
//  PromoDetailShopNowTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/30/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class PromoDetailShopNowTableCellView: UITableViewCell {
    @IBOutlet weak var btnAction: UIButton!
    
    func buildLayout() {
        btnAction.layer.cornerRadius = btnAction.frame.size.height/2
        btnAction.layer.masksToBounds = true
        
    }
    
}