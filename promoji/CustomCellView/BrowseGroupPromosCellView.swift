//
//  BrowseGroupPromosCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/22/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class BrowseGroupPromosCellView : UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    func buildLayout() {
        self.imageView.layer.cornerRadius = 8.0
        self.imageView.layer.masksToBounds = true
        self.imageView.setNeedsLayout()
        self.imageView.setNeedsUpdateConstraints()
    }
}