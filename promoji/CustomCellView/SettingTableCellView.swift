//
//  SettingTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 9/10/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class SettingTableCellView : UITableViewCell {
    
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    
    func buildLayout() {
        iconView.layer.cornerRadius = 4.0
        iconView.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}