//
//  BrowseMainTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/22/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class BrowseMainTableCellView: UITableViewCell {
    @IBOutlet weak var slideView: UIView!
    @IBOutlet weak var indicatorHolderView: UIView!
    @IBOutlet weak var indicatorView: UIView!
    
//    private var pageViewController: UIPageViewController?
//    var pageController: UIPageViewController?
    
    func buildLayout() {
//        if pageViewController != nil {
//            pageViewController?.willMoveToParentViewController(nil)
//            pageViewController?.view.removeFromSuperview()
//            pageViewController?.removeFromParentViewController()
//            pageViewController?.delegate = nil
//            pageViewController = nil
//        }
//        pageController!.dataSource = self
//        
//        pageViewController = pageController!
//        pageViewController?.view.frame = self.slideView.frame
//        pageViewController?.delegate = self
//        
//        addChildViewController(pageViewController!)
//        self.slideViewHolder.addSubview(pageViewController!.view)
//        pageViewController!.didMoveToParentViewController(self)
    }
    
//    func nextPage() {
//        if self.fetchedResultsFeaturedController()!.sections![0].numberOfObjects > 1 {
//            let itemController = self.pageViewController?.viewControllers!.last as! FeaturedNewsItemController
//            var firstController : FeaturedNewsItemController
//            if itemController.itemIndex+1 < self.fetchedResultsFeaturedController()!.sections![0].numberOfObjects {
//                self.styledPageControl?.currentPage = itemController.itemIndex + 1
//                firstController = getItemController(itemController.itemIndex+1)!
//            } else {
//                self.styledPageControl?.currentPage = 0
//                firstController = getItemController(0)!
//            }
//            self.pageViewController!.setViewControllers([firstController], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)
//        }
//        
//    }
//    
//    private func getItemController(itemIndex: Int) -> FeaturedNewsItemController? {
//        if itemIndex < self.fetchedResultsFeaturedController()!.sections![0].numberOfObjects {
//            let featuredNewsController = self.storyboard!.instantiateViewControllerWithIdentifier("ItemController") as! FeaturedNewsItemController
//            featuredNewsController.itemIndex = itemIndex
//            
//            let indexPath: NSIndexPath = NSIndexPath(forRow: itemIndex, inSection: 0)
//            let newmodel: NewsModel? = self.fetchedResultsFeaturedController()!.objectAtIndexPath(indexPath) as? NewsModel
//            featuredNewsController.imageURL = UtilHelper.parseStringURL(newmodel!.image_featured!)
//            featuredNewsController.newstitle = newmodel!.title!
//            
//            let dateString = UtilHelper.parseDateToStringFromPreviousDateWithTimeToday(newmodel!.date!)
//            featuredNewsController.datedescription = dateString
//            
//            
//            let articletypeid = newmodel!.article_type_name!
//            if articletypeid == "0" {
//                featuredNewsController.frameBackgroundColor = UIColor(red: 106/255, green: 61/255, blue: 141/255, alpha: 1)
//            } else if articletypeid == "1" {
//                featuredNewsController.frameBackgroundColor = UIColor(red: 81/255, green: 160/255, blue: 92/255, alpha: 1)
//            } else if articletypeid == "2" {
//                featuredNewsController.frameBackgroundColor = UIColor(red: 107/255, green: 156/255, blue: 182/255, alpha: 1)
//            } else if articletypeid == "3" {
//                featuredNewsController.frameBackgroundColor = UIColor(red: 247/255, green: 198/255, blue: 0/255, alpha: 1)
//            }
//            
//            
//            featuredNewsController.view.tag = itemIndex
//            let gestureRecognize = UITapGestureRecognizer(target: self, action: "selectRow:")
//            featuredNewsController.view.addGestureRecognizer(gestureRecognize)
//            
//            return featuredNewsController
//        }
//        return nil
//    }
//    
//    func selectRow(sender: UIGestureRecognizer) {
//        let tag = sender.view?.tag
//        let indexPath: NSIndexPath = NSIndexPath(forRow: tag!, inSection: 0)
//        
//        let newmodel: NewsModel? = self.fetchedResultsFeaturedController()!.objectAtIndexPath(indexPath) as? NewsModel
//        //        openWebView(NSURL(string: NEWS_DETAIL+newmodel!.article_id!+"?qpocket_news")!, title: "News Detail")
//        openWebView(NSURL(string: "http://qeon.co.id/news/detailnews/"+newmodel!.article_id!+"?_s=qpl")!, title: "News Detail")
//    }
//    
//    // MARK: - UIPageViewControlerDataSource
//    
//    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
//        let itemController = viewController as! FeaturedNewsItemController
//        
//        if itemController.itemIndex > 0 {
//            return getItemController(itemController.itemIndex-1)
//        }
//        return nil
//    }
//    
//    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
//        let itemController = viewController as? FeaturedNewsItemController
//        
//        if itemController != nil {
//            if itemController!.itemIndex+1 < self.fetchedResultsFeaturedController()!.sections![0].numberOfObjects {
//                return getItemController(itemController!.itemIndex+1)
//            }
//        }
//        
//        return nil
//    }
//    
//    
//    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
//        if !completed {
//            return
//        }
//        
//        let itemController = self.pageViewController?.viewControllers!.last as! FeaturedNewsItemController
//        self.styledPageControl?.currentPage = itemController.itemIndex
//    }
    
}