//
//  PromoDetailsSecondInfoTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/30/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class PromoDetailsSecondInfoTableCellView: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    
    func buildLayout() {
        bgView.layer.cornerRadius = 8
        bgView.layer.masksToBounds = true
        
    }
    
}