//
//  BrandDetailsConnectTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/28/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class BrandDetailsConnectTableCellView : UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnFacebook: UIView!
    @IBOutlet weak var btnTwitter: UIButton!
    @IBOutlet weak var btnGPlus: UIButton!
    @IBOutlet weak var btnInstagram: UIButton!
    
    func buildLayout() {
        bgView.layer.cornerRadius = 8.0
        bgView.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}