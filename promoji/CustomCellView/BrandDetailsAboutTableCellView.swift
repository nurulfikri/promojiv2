//
//  BrandDetailsAboutTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/28/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class BrandDetailsAboutTableCellView : UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblAbout: UILabel!
    
    func buildLayout() {
        bgView.layer.cornerRadius = 8.0
        bgView.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}