//
//  BrandUpdatesTextTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/28/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation
import UIKit

class BrandUpdatesTextTableCellView : UITableViewCell {
    
    @IBOutlet weak var lblAccountName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var imageBrand: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    func buildLayout() {
        bgView.layer.cornerRadius = 8.0
        bgView.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}