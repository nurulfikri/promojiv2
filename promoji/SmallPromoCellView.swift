//
//  SmallPromoCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/26/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class SmallPromoCellView: UITableViewCell {
    
    @IBOutlet weak var merchantImage: UIImageView!
    @IBOutlet weak var promoName: UILabel!
    @IBOutlet weak var descriptionHolder: UIView!
    @IBOutlet weak var detailHolder: UIView!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var holderBottom: UIView!
    
    var merchantPromo = ""
    var promoterString = ""
    var merchantSelector: Selector?
    var promoSelector: Selector?
    
    func buildLayout() {
        holderBottom.layer.cornerRadius = 8.0
        holderBottom.layer.masksToBounds = true
        
        
        UIView().buildTextClickable("by #\(merchantPromo)# with #\(promoterString)", linkString: [merchantPromo:"", promoterString:""], font: UIFont.systemFont(ofSize: 14), textColor: UIColor.darkGray, textClickableColor: UIColor.blue, holderView: descriptionHolder)
        
        UIView().buildTextClickable("Valid until 26 Dec 2016", linkString: ["":""], font: UIFont.systemFont(ofSize: 14), textColor: UIColor.darkGray, textClickableColor: UIColor.blue, holderView: detailHolder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        buildLayout()
    }
    
}
