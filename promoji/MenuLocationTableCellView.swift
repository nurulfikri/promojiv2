//
//  MenuLocationTableCellView.swift
//  promoji
//
//  Created by Nurul Fikri on 8/23/16.
//  Copyright © 2016 Promoji. All rights reserved.
//

import Foundation

class MenuLocationTableCellView: UITableViewCell {
    @IBOutlet weak var lblPromos: UILabel!
    @IBOutlet weak var lblPlace: UILabel!
    @IBOutlet weak var imageVIew: UIImageView!
    let gradientLayer = CAGradientLayer()
    
    func buildLayout() {
        gradientLayer.removeFromSuperlayer()
        gradientLayer.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        gradientLayer.colors = [UIColor(white: 0.0, alpha: 0.0).cgColor,
                                UIColor(white: 0.0, alpha: 0.4).cgColor]
        
        self.imageVIew.layer.addSublayer(gradientLayer)
        
        self.imageVIew.layer.cornerRadius = 8.0
        self.imageVIew.layer.masksToBounds = true
        self.imageVIew.setNeedsLayout()
        self.imageVIew.setNeedsUpdateConstraints()
    }
}
